#include "parser.h"
#include "interpret.h"
#include "stdio.h"
#include "stdarg.h"
#include "symbol.h"
#include "global.h"

#define LIST_CUSTOM_NAME eStack
#define LIST_DATA_TYPE expr_sym
#define LIST_DEFAULT_VAL {expr_size, type_eof, NULL}
#include "list_base.ct"
#include "token.h"

#undef LIST_DEFAULT_VAL
#undef LIST_DATA_TYPE
#undef LIST_CUSTOM_NAME

#define LIST_CUSTOM_NAME oStack
#define LIST_DATA_TYPE int
#define LIST_DEFAULT_VAL 0
#include "list_base.ct"
#undef LIST_DEFAULT_VAL
#undef LIST_DATA_TYPE
#undef LIST_CUSTOM_NAME

static token_t token;
static string scope;
static vec_instr* code_buffer;

symbol_ptr current_fun;

static token_t next()
{
    if(token.type == type_identifier || token.type == type_identifier_fun)
        tstr_free(token.identifier);
    token = get_token();
    return token;
}

expr_type expr_index(token_t t)
{
    switch(t.type)
    {
        case type_identifier:
        case type_int_lit:
        case type_double_lit:
        case type_string_lit:
            return expr_var;
        case type_bracket_open:
            return expr_bracket_open;
        case type_bracket_close:
            return expr_bracket_close;
        case type_star:
        case type_slash:
            return expr_op_3;
        case type_plus:
        case type_minus:
            return expr_op_6;
        case type_greater:
        case type_lesser:
        case type_lequal:
        case type_gequal:
        case type_dequal:
        case type_nequal:
            return expr_op_9;
        case type_comma:
            return expr_comma;
        case type_eof:
            return expr_end;
        default:
            //LOG_DBG("Unknown token type %d (%s) !\n", t.type, token_type_name(t.type));
            return -1;
    }
    return -1;// Error, unknown token , probably should ignore it and continue
}

//Addressing is expr_table[row][col]
int expr_table[expr_end][expr_end] = {
//      var    fun     (       )       */      +-      ==      ,
/*var*/ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*fun*/ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*(  */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*)  */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*/* */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*+- */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*== */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
/*,  */ {MATCH, MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH,  MATCH},
};

int expr_table2[expr_size][expr_size] = {
//      var    fun     (       )       */      +-      ==      ,       $
/*var*/ {PTERR, PTERR,  PTFUN,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC},
/*fun*/ {SHIFT, SHIFT,  SHIFT,  DEFUN,  SHIFT,  SHIFT,  SHIFT,  PTFUN,  PTERR},
/*(  */ {SHIFT, SHIFT,  SHIFT,  MATCH,  SHIFT,  SHIFT,  SHIFT,  REDUC,  PTERR},
/*)  */ {PTERR, PTERR,  PTERR,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC},
/*/* */ {SHIFT, SHIFT,  SHIFT,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC,  REDUC},
/*+- */ {SHIFT, SHIFT,  SHIFT,  REDUC,  SHIFT,  REDUC,  REDUC,  REDUC,  REDUC},
/*== */ {SHIFT, SHIFT,  SHIFT,  REDUC,  SHIFT,  SHIFT,  REDUC,  REDUC,  REDUC},
/*,  */ {PTERR, PTERR,  PTERR,  PTERR,  PTERR,  PTERR,  PTERR,  PTERR,  PTERR},
/* $ */ {SHIFT, SHIFT,  SHIFT,  PTERR,  SHIFT,  SHIFT,  SHIFT,  THROW,  PTDON},
};

/*
 * Convert expr_type to printable string
 *
 * @param type Type to convert.
 * @return Returns printable string.
 */
const char* expr_type_name(expr_type type)
{
    switch(type)
    {
        case expr_mark:
            return "<";
        case expr_non_term_var :
            return "Non-terminal variable";
        case expr_non_term :
            return "Non-terminal";
        case expr_var :
            return "Variable";
        case expr_fun :
            return "Function";
        case expr_bracket_open :
            return "Opening bracket";
        case expr_bracket_close :
            return "Closing bracket";
        case expr_op_3 :
            return "Operator with priority 3";
        case expr_op_6 :
            return "Operator with priority 6";
        case expr_op_9 :
            return "Operator with priority 9";
        case expr_comma :
            return "Comma";
        case expr_end :
            return "End of file";
        case expr_size :
        default :
            return "ERROR, unknown type";
    }
}

/*
 * Prints the content of given stack.
 *
 * @param stack Stack to print out.
 */
void parse_debug_stack(eStack *stack)
{
#ifdef DEBUG
    eStack_first(stack);
    expr_sym eSym;

    fprintf(stderr, "/=========PARSING_STACK==========\\\n");
    while(eStack_active(stack))
    {
        eSym = eStack_get(stack);

        fprintf(stderr, "<Symbol>\n");
        fprintf(stderr, "\tType: %s\n", expr_type_name(eSym.type));
        fprintf(stderr, "\tToken type: %s\n", token_type_name(eSym.tokType));
        fprintf(stderr, "\tSym: %p\n", eSym.sym);
        fprintf(stderr, "</Symbol>\n");

        eStack_succ(stack);
    }
    fprintf(stderr, "\\================================/\n");
#endif
}

/*
 * Activates the topmost terminal on given eStack. If there are no terminals, stack becomes non-active.
 *
 * @param stack Stack to operate on.
 */
void expr_activate_topmost_term(eStack *stack)
{
    expr_sym data = {expr_size, type_eof, NULL};

    eStack_first(stack);

    while(eStack_active(stack))
    {
        data = eStack_get(stack);
        if(data.type > expr_non_term && data.type < expr_size)
            return;
        eStack_succ(stack);
    }
}

/*
 * Get the topmost terminal from given eStack.
 *
 * @param stack Stack to search.
 * @return Returns the topmost terminal. If there are no terminals found, returns expr_size, as the type.
 */
expr_sym expr_get_topmost_term(eStack *stack)
{
    expr_sym data = {expr_size, type_eof, NULL};

    expr_activate_topmost_term(stack);

    if(eStack_active(stack))
        data = eStack_get(stack);

    return data;
}

/*
 * Push given data behind the first terminal found on given eStack. If there are no terminal, exits the program!
 *
 * @param stack Stack to push onto.
 * @param data Data to push onto the stack.
 */
void expr_push_topmost_term(eStack *stack, expr_sym data)
{
    expr_activate_topmost_term(stack);

    if(eStack_active(stack))
        eStack_pre_insert(stack, data);
    else
        fail(ERR_SYNTAX, "Expression parsing failed - no terminals found", token.line_num, token.char_num);

}

/*
 * Parse expression, append code to code_buffer
 *
 * @param stack Stack to push onto.
 * @param data Data to push onto the stack.
 * @returns symbol_ptr - result of the expression
 */
symbol_ptr parse_expr(int end1,int end2)
{
    // Remove this line, used for testing!
    //next();
    // Remove the line above, if not testing expressions like "(a+b)"



    eStack *stack = eStack_init();
    oStack *funStack = oStack_init();
    expr_sym data = {expr_end, type_eof, NULL};
    expr_sym data2 = {expr_end, type_eof, NULL};
    expr_sym data3 = {expr_end, type_eof, NULL};
    expr_sym mark = {expr_end, type_eof, NULL};
    eStack_push(stack, data);
    data.type = expr_size;
    int indexOnStack = -1;
    int indexOnInput = -1;
    int action = PTERR;
    bool running = true;
    bool finish = false;

    next();

    while(running)
    {
        indexOnStack = expr_get_topmost_term(stack).type;
        if(indexOnStack == expr_size)
        {
            fail(ERR_SYNTAX, "Expression parsing failed - no terminal", token.line_num, token.char_num);
            /*
            fprintf(stderr, "ERROR, no terminals were found!\n");
            parse_debug_stack(stack);
            exit(-1);
            */
        }

        indexOnInput = expr_index(token);
        if(indexOnInput == -1 && !finish)
            action = PTERR;
        else if(finish)
            /* If one of the ending symbols were found */
            if(indexOnStack == expr_end)
                action = PTDON; // There are no more expressions to reduce
            else
                action = expr_table2[indexOnStack][expr_end]; // Reduce all remaining expressions
        else
            action = expr_table2[indexOnStack][indexOnInput];

        switch(action)
        {
            case SHIFT:
                LOG_DBG("Shifting... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));

                data.type = expr_mark;
                data.tokType = type_eof;
                data.sym = NULL;
                expr_push_topmost_term(stack, data);

                symbol_ptr var = NULL;

                switch(token.type)
                {
                    /* We got an ID of a variable */
                    case type_identifier:
                        var = cg_get_var(token.identifier);
                        if(var == NULL)
                            var = lookup_fun(token.identifier);
                        break;
                    case type_int_lit:
                        var = sym_gen_const();
                        var->constant.type = type_kw_int;
                        var->constant.int_val = token.int_lit;
                        break;
                    case type_double_lit:
                        var = sym_gen_const();
                        var->constant.type = type_kw_double;
                        var->constant.double_val = token.double_lit;
                        break;
                    case type_string_lit:
                        var = sym_gen_const();
                        var->constant.type = type_kw_string;
                        var->constant.string_val = token.string_lit;
                        break;
                    default:
                        break;
                }

                data.type = indexOnInput;
                data.tokType = token.type;
                data.sym = var;

                eStack_push(stack, data);

                next();
                break;
            case REDUC:
                LOG_DBG("Reducing... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));
                expr_activate_topmost_term(stack);
                data = eStack_get(stack);

                switch(data.type)
                {
                    case expr_var:
                        LOG_DBG("Reducing variable!", NULL);

                        /* Variable itself */
                        data = eStack_pop(stack);

                        /* Reduction mark */
                        data2 = eStack_pop(stack);

                        /* Test, if the symbols on the stack correspond to the operators operands */
                        if(data.type != expr_var || data2.type != expr_mark)
                        {
                            LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(var)", NULL);
                            fail(ERR_SYNTAX, "Expression parsing failed - wrong terminal types(var)", token.line_num, token.char_num);
                            /*
                            parse_debug_stack(stack);
                            exit(-1);
                            */
                        }

                        /* Test, if the symbol was actually found in the symbol table */
                        if(data.sym == NULL)
                            fail(ERR_SEMANTIC, "Expression parsing failed - undeclared variable used", token.line_num, token.char_num);
                        if(data.sym->type == sym_function)
                            fail(ERR_SEMANTIC_OTHER, "Expression parsing failed - undeclared variable used", token.line_num, token.char_num);

                        /* If the symbol is a constant, copy it first */
                        /*
                        if(data.sym->type == sym_const)
                        {
                            symbol_ptr newSym = cg_gen_temp_var();
                            newSym->variable.type = data.sym->constant.type;
                            vec_instr_push(code_buffer, instr_gen(INSTR_MOV, newSym, data.sym, NULL));
                            data.sym = newSym;
                        }
                         */

                        /* Return it to the stack */
                        data.type = expr_non_term_var;
                        data.tokType = type_eof;
                        eStack_push(stack, data);
                        break;
                    case expr_op_3:
                    case expr_op_6:
                    case expr_op_9:
                        LOG_DBG("Reducing expression, using operator %s.", token_type_name(data.tokType));

                        /* First operand */
                        data = eStack_pop(stack);

                        /* Operator */
                        data2 = eStack_pop(stack);

                        /* Second operand */
                        data3 = eStack_pop(stack);

                        /* Reduction mark */
                        mark = eStack_pop(stack);

                        /* Test, if the symbols on the stack correspond to the operators operands */
                        if(data.type != expr_non_term_var || !expr_type_is_op(data2.type) ||
                           data3.type != expr_non_term_var || mark.type != expr_mark)
                        {
                            LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(op,br)", NULL);
                            fail(ERR_SYNTAX, "Expression parsing failed - wrong terminal types(op,br)", token.line_num, token.char_num);
                            /*
                            parse_debug_stack(stack);
                            exit(-1);
                            */
                        }

                        /* Generate temporary variable here */
                        symbol_ptr tempSym = cg_gen_temp_var();

                        /* Generate the instructions, to fill the new symbol HERE */
                        token_type resType = type_eof;
                        LOG_DBG("Operation %s", token_type_name(data2.tokType));
                        /*
                        switch(data2.tokType)
                        {
                            case type_plus:
                            case type_minus:
                            case type_star:
                            case type_slash:
                            case type_dequal:
                            case type_nequal:
                            case type_lequal:
                            case type_gequal:
                            case type_greater:
                            case type_lesser:
                            */
                                LOG_DBG("Operating", NULL);
                                /* If any of the operands are constants, if they are, copy them first */
                                if(data.sym->type == sym_const)
                                {
                                    symbol_ptr newSym = cg_gen_temp_var();
                                    newSym->variable.type = data.sym->constant.type;
                                    vec_instr_push(code_buffer, instr_gen(INSTR_MOV, newSym, data.sym, NULL));
                                    data.sym = newSym;
                                }
                                if(data3.sym->type == sym_const)
                                {
                                    symbol_ptr newSym = cg_gen_temp_var();
                                    newSym->variable.type = data3.sym->constant.type;
                                    vec_instr_push(code_buffer, instr_gen(INSTR_MOV, newSym, data3.sym, NULL));
                                    data3.sym = newSym;
                                }
                                resType = get_res_type(data.sym, data3.sym);
                                if(resType == type_eof)
                                    fail(ERR_TYPE_MISMATCH, "Expression parsing failed - operation not allowed on given operands",
                                         token.line_num, token.char_num);
                                if(resType == type_kw_string && data2.tokType != type_dequal && data2.tokType != type_nequal)
                                    fail(ERR_TYPE_MISMATCH, "Expression parsing failed - operation not allowed on given operands",
                                         token.line_num, token.char_num);
                                symbol_ptr op1, op2;
                                if(data.sym->variable.type == type_kw_string && data3.sym->variable.type == type_kw_string)
                                {
                                    tempSym->variable.type = type_kw_int;
                                    op1 = type_match(type_kw_string, data3.sym, code_buffer);
                                    op2 = type_match(type_kw_string, data.sym, code_buffer);
                                }
                                else
                                {
                                    tempSym->variable.type = resType;
                                    op1 = type_match(resType, data3.sym, code_buffer);
                                    op2 = type_match(resType, data.sym, code_buffer);
                                }
                                tempSym = cg_gen_ar_instr(tempSym, op1, op2, data2.tokType, code_buffer);
                                /*break;
                            default:
                                fail(ERR_SYNTAX, "Expression parsing failed - unknown operator", token.line_num, token.char_num);
                                break;
                        }
                         */

                        /* Done with generating instructions */
                        data.type = expr_non_term_var;
                        data.tokType = type_identifier;
                        data.sym = tempSym;
                        eStack_push(stack, data);
                        break;
                    case expr_bracket_close:
                        LOG_DBG("Reducing expression, using operator %s.", token_type_name(data.tokType));

                        /* ( */
                        data = eStack_pop(stack);

                        /* variable */
                        data2 = eStack_pop(stack);

                        /* ) */
                        data3 = eStack_pop(stack);

                        /* Reduction mark */
                        mark = eStack_pop(stack);

                        /* Test, if the symbols on the stack correspond to the operators operands */
                        if(data.type != expr_bracket_close || data2.type != expr_non_term_var ||
                           data3.type != expr_bracket_open || mark.type != expr_mark)
                        {
                            LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(op,br)", NULL);
                            fail(ERR_SYNTAX, "Expression parsing failed - wrong terminal types(op,br)", token.line_num, token.char_num);
                            /*
                            parse_debug_stack(stack);
                            exit(-1);
                            */
                        }

                        data2.type = expr_non_term_var;
                        data2.tokType = type_identifier;
                        eStack_push(stack, data2);
                        break;
                    default:
                        LOG_DBG("Reducing for this symbol is not implemented yet!", NULL);
                        break;
                }
                break;
            case MATCH:
                LOG_DBG("Matching... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));

                /* Just put the symbol onto the stack */
                data.type = indexOnInput;
                data.tokType = token.type;
                data.sym = NULL;
                eStack_push(stack, data);

                next();
                break;
            case PTFUN:
                LOG_DBG("Creating fun terminal... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));

                if(indexOnInput == expr_bracket_open)
                { /* Creating the first function symbol => myFun( */
                    data = eStack_pop(stack);

                    /* Test, if the symbol on the stack correspond to the operators operands */
                    if(data.type != expr_var)
                    {
                        LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(fun1)", NULL);
                        fail(ERR_SYNTAX, "Expression parsing failed - incorrect terminal found", token.line_num, token.char_num);
                        /*
                        parse_debug_stack(stack);
                        exit(-1);
                        */
                    }

                    /* Test, if the function has been declared */
                    if(data.sym == NULL)
                        fail(ERR_SEMANTIC, "Expression parsing failed - undeclared function used", token.line_num, token.char_num);
                    if(data.sym->type != sym_function)
                        fail(ERR_SEMANTIC_OTHER, "Expression parsing failed - cannot use variable as function", token.line_num, token.char_num);
                    /* Create the function symbol */
                    data.type = expr_fun;
                    data.tokType = type_identifier_fun;
                    eStack_push(stack, data);

                    /* A
                     *  the instruction, for creating prep frame */
                    vec_instr_push(code_buffer, instr_gen(INSTR_FR_PREP, data.sym, NULL, NULL));

                    /* Create new record, used for param checking */
                    oStack_push(funStack, 0);
                }
                else if(indexOnInput == expr_comma)
                { /* Reducing already calculated parameter into the function symbol => fun_sym param , */
                    data = eStack_pop(stack);
                    data2 = eStack_top(stack);

                    /* Test, if the symbol on the stack correspond to the operators operands */
                    if(data.type != expr_non_term_var || data2.type != expr_fun)
                    {
                        LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(fun2)", NULL);
                        fail(ERR_SYNTAX, "Expression parsing failed - incorrect terminal found", token.line_num, token.char_num);
                        /*
                        parse_debug_stack(stack);
                        exit(-1);
                        */
                    }

                    /* Process the data here, it contains one of the function parameters */
                    symbol_ptr param = data.sym;
                    int funParamOffset = oStack_pop(funStack);
                    if(funParamOffset >= vec_symbol_ptr_size(data2.sym->function.params))
                        fail(ERR_TYPE_MISMATCH, "Expression parsing failed - calling function with too many parameters",
                             token.line_num, token.char_num);
                    symbol_ptr funParam = *vec_symbol_ptr_at(data2.sym->function.params, funParamOffset);
                    param = type_match(funParam->variable.type, param, code_buffer);
                    vec_instr_push(code_buffer, instr_gen(INSTR_FR_PARAM, param, funParam->variable.mem_offset, NULL));
                    oStack_push(funStack, funParamOffset + 1);

                    /* Function symbol should be on the top now */

                    /*
                    eStack_pop(stack);

                    data.type = expr_fun;
                    data.tokType = type_identifier_fun;
                    data.sym = NULL;
                    eStack_push(stack, data);
                     */
                }

                next();
                break;
            case DEFUN:
                LOG_DBG("Reducing function to variable... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));

                data = eStack_pop(stack);

                /* If there is one more parameter to reduce, do it */
                if(data.type == expr_non_term_var)
                {
                    /* Move the reduced symbol to data2 */
                    data2 = data;
                    /* Pop the function symbol */
                    data = eStack_pop(stack);

                    /* Test, if the symbol on the stack correspond to the operators operands */
                    if(data2.type != expr_non_term_var || data.type != expr_fun)
                    {
                        LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(fun2)", NULL);
                        fail(ERR_SYNTAX, "Expression parsing failed - incorrect terminal found", token.line_num, token.char_num);
                    }

                    /* Process the last data here */
                    symbol_ptr param = data2.sym;
                    int funParamOffset = oStack_pop(funStack);
                    if(funParamOffset >= vec_symbol_ptr_size(data.sym->function.params))
                        fail(ERR_TYPE_MISMATCH, "Expression parsing failed - calling function with too many parameters",
                             token.line_num, token.char_num);
                    symbol_ptr funParam = *vec_symbol_ptr_at(data.sym->function.params, funParamOffset);
                    param = type_match(funParam->variable.type, param, code_buffer);
                    vec_instr_push(code_buffer, instr_gen(INSTR_FR_PARAM, param, funParam->variable.mem_offset, NULL));
                    oStack_push(funStack, funParamOffset + 1);
                }

                /* data contains the function symbol here */

                if(data.type != expr_fun)
                {
                    LOG_DBG("ERROR, one of the reduced symbols does not have the correct type!(redfun)", NULL);
                    fail(ERR_SYNTAX, "Expression parsing failed - function terminal not found",
                         token.line_num, token.char_num);
                    /*
                    parse_debug_stack(stack);
                    exit(-1);
                    */
                }

                /* Test, if all the parameters were supplied */
                int funParamOffset = oStack_pop(funStack);
                if(funParamOffset != vec_symbol_ptr_size(data.sym->function.params))
                    fail(ERR_TYPE_MISMATCH, "Expression parsing failed - not enough parameters supplied",
                         token.line_num, token.char_num);

                /* Create new temporary symbol, used for return value */
                symbol_ptr tempVar = cg_gen_temp_var();

                tempVar->variable.type = data.sym->function.ret_type;

                /* Add the call instruction */
                vec_instr_push(code_buffer, instr_gen(INSTR_FR_CALL, tempVar, NULL, NULL));

                /* Add a variable symbol instead */
                /* This variable will be reduced in the next step to a expr_non_term_var */
                data.type = expr_var;
                data.tokType = type_identifier;
                data.sym = tempVar;
                eStack_push(stack, data);

                next();
                break;
            case PTDON:
                LOG_DBG("Done with expression!", NULL);
                running = false;
                break;
            case THROW:
                LOG_DBG("Throwing current symbol away... %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));
                eStack_pop(stack);

                next();
                break;
            case PTERR:
                if((token.type == end1 || token.type == end2) && !finish)
                {
                    LOG_DBG("Found one of the ending symbols %s", token_type_name(token.type));
                    finish = true;
                }
                else
                {
                    LOG_DBG("ERROR occurred %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));
                    parse_debug_stack(stack);
                    fail(ERR_SYNTAX,"Unexpected token",token.line_num,token.char_num);
                }
                break;
            default:
                LOG_DBG("ERROR unknown action %s %s", expr_type_name(indexOnStack), expr_type_name(indexOnInput));
        }

        parse_debug_stack(stack);
        //printf("Press enter, to continue!\n");
        //getchar();
    }

    if(eStack_size(stack) != 2)
    {
        LOG_DBG("ERROR ending, and the size of the stack is not 2, it is in fact %ld!", eStack_size(stack));
        fail(ERR_SYNTAX, "Unable to parse expression", token.line_num, token.char_num);
        //parse_debug_stack(stack);
        //exit(-1);
    }

    LOG_DBG("Expression successfuly parsed!", NULL);

    symbol_ptr retSym = eStack_pop(stack).sym;
    sym_debug(retSym);

    eStack_free(stack);
    oStack_free(funStack);

    return retSym;
#if 0
    vec_int *identifiers = vec_int_init();
    vec_symbol_ptr *symbolData = vec_symbol_ptr_init();

    vec_int_push(identifiers, expr_end);
    vec_symbol_ptr_push(symbolData, NULL);

    bool running = true;
    int op = -1;
    int indexOnStack = 0;
    int indexOnInput = 0;

    while(running)
    {
        next();
        if(token.type == end1 || token.type == end2)
        {
            running = false;
        }

        indexOnStack = *vec_int_top(identifiers);
        indexOnInput = expr_index(token);

        op = expr_table2[indexOnStack][indexOnInput];

        switch(op)
        {
            case PTERR:
                LOG_DBG("Error in expression parsing, unaccepted combination of stack and input token!\n");
                exit(-1);
                break;
            case MATCH: /* = */
                vec_int_push(identifiers, indexOnInput);
                break;
            case SHIFT: /* < */
                break;
            case REDUC: /* > */
                break;
            default:
                break;
        }

    }

    vec_int_free(identifiers);
    vec_symbol_ptr_free(symbolData);

    LOG_DBG("Expr ended, %d", token.type);
#endif
}

/* Format of parsing functions
    first if/switch corresponds to recognizing input symbol ( individual columns in predict table)
    if there is a match, next step is to replace left side of rule with right side:

    all nonterminals(tokens) are removed from input and saved on the stack ( variables in functions )
    and all terminals are processed as calling function corresponding to nonterminal, 

    Code generation can be done on the stack, or in shared state 
*/
bool parser_init()
{
    cg_init();

    /* Add function prototype for main function -> int main(); */
    symbol_ptr mainFun = sym_gen_func();
    string mainFunName = c_str_to_str("main");
    mainFun->function.ret_type = type_kw_int;
    insert_func_sym(mainFunName, mainFun);
    tstr_free(mainFunName);

    /* Add internal functions */
    /* int length(string s), string substr(string s, int i, int n), string concat(string s1, string s2),
     * int find(string s, string search), string sort(string s) */
    symbol_ptr param;

    /* LENGTH */
    symbol_ptr iLength = sym_gen_ifunc(type_kw_int, 1, caller_length);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 0;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iLength->function.params, param);

    /* SUBSTR */
    symbol_ptr iSubstr = sym_gen_ifunc(type_kw_string, 3, caller_substr);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 0;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iSubstr->function.params, param);
    param = sym_gen_var();
    param->variable.type = type_kw_int;
    param->variable.mem_offset = 1;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iSubstr->function.params, param);
    param = sym_gen_var();
    param->variable.type = type_kw_int;
    param->variable.mem_offset = 2;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iSubstr->function.params, param);

    /* CONCAT */
    symbol_ptr iConcat = sym_gen_ifunc(type_kw_string, 2, caller_concat);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 0;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iConcat->function.params, param);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 1;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iConcat->function.params, param);

    /* FIND */
    symbol_ptr iFind = sym_gen_ifunc(type_kw_int, 2, caller_find);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 0;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iFind->function.params, param);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 1;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iFind->function.params, param);

    /* SORT */
    symbol_ptr iSort = sym_gen_ifunc(type_kw_string, 1, caller_sort);
    param = sym_gen_var();
    param->variable.type = type_kw_string;
    param->variable.mem_offset = 0;
    param->variable.name = NULL;
    vec_symbol_ptr_push(iSort->function.params, param);

    /* Add them to the symbol table */
    string name = c_str_to_str("length");
    insert_func_sym(name, iLength);
    tstr_free(name);

    name = c_str_to_str("substr");
    insert_func_sym(name, iSubstr);
    tstr_free(name);

    name = c_str_to_str("concat");
    insert_func_sym(name, iConcat);
    tstr_free(name);

    name = c_str_to_str("find");
    insert_func_sym(name, iFind);
    tstr_free(name);

    name = c_str_to_str("sort");
    insert_func_sym(name, iSort);
    tstr_free(name);

    return true;
}

void parser_free()
{
    cg_free();
}

void program()
{
    next();
    if(!is_token_type(token))
        fail(ERR_SYNTAX, "No type found", token.line_num, token.char_num);
    while(is_token_type(token))
    {
        fun_dd();
    }
    if(token.type != type_eof)
    {
        fail(ERR_SYNTAX,"Expected end-of-file ",token.line_num,token.char_num);
    }

}

void fun_dd()
{
  
    token_t fun_type;
    token_t fun_name;
    token_t bracket_open;
    token_t bracket_close;
    if(is_token_type(token))
    {
        fun_type = token;
        if(next_token_expect(fun_name,type_identifier))
            cg_new_fun(fun_type.type, tstr_duplicate(fun_name.identifier));
            if(next_token_expect(bracket_open,type_bracket_open))
            {
                next(); 
                // we need to call next(), so all functions can check current token as first action
                //instead of next one. This could lead to some errors with "epsilon" rules

                param();

                if (this_token_expect(bracket_close,type_bracket_close))
                {   

                    next();
                    f_spec();
                    cg_fun_ended();
                    return;
                }
            }
    }
    fail(ERR_SYNTAX,"Unexpected Token in function signature",token.line_num,token.char_num);
}



void param()
{
    token_t p_type;
    token_t p_name;
    if(is_token_type(token))
    {
        p_type = token;

        if(next_token_expect(p_name,type_identifier))
        {
            cg_fun_param(p_type.type,p_name.identifier);
            LOG_DBG("Param,%s",str_to_c_str(p_name.identifier));
            next();
            param_l();
            return;            
        }
    }
    else if(token.type == type_bracket_close)
    {
        //No params
        return;
    }
    fail(ERR_SYNTAX,"Unexpected Token in parameter definition",token.line_num,token.char_num);

}
void param_l()
{ 
    token_t p_type;
    token_t p_name;
    if(token.type==type_comma)
    {    
        p_type=next();
        if(is_token_type(token))
        {
            p_name=next();
            if(p_name.type == type_identifier)
            {                
                cg_fun_param(p_type.type,p_name.identifier);
                next(); 
                param_l();
                return;
            }
        }
    }
    else if(token.type == type_bracket_close)
    { 
        LOG_DBG("+Param_l,ended",0);
        return;
    }
    fail(ERR_SYNTAX,"Unexpected Token in parameter definition",token.line_num,token.char_num);
}



void f_spec()
{
    if(token.type == type_semicolon)
    {
        next();
        LOG_DBG("Declared",0);
        cg_fun_declared();
        return;
    } 
    else if ( token.type == type_brace_open)
    {
        //We should utilize info in params for stack frame creation( number and size of parameters)

        LOG_DBG("Defined ",0);
        cg_fun_defined();
        cmp_st();

        /* Add function ending symbol */
        vec_instr_push(current_fun->function.code,instr_gen(INSTR_FUN_END,NULL,NULL,NULL));

        return;
    }
    fail(ERR_SYNTAX,"Unexpected Token in function definition/declaration",token.line_num,token.char_num);
}

void cmp_st()
{
    if(token.type == type_brace_open)
    {
        next();
        cg_scope_enter();
        st_list();
        if(token.type == type_brace_close)
        {
            next();
            cg_scope_exit();
            return;
        }
    }
    fail(ERR_SYNTAX,"Unexpected Token in compound statement",token.line_num,token.char_num);
}

void st_list()
{
    while(token.type != type_brace_close)
    {
        st();
    }
}

void st()
{
    switch(token.type)
    {
        case type_identifier:
            st_assign(type_semicolon);
            if(token.type == type_semicolon)
            {
                next();
                return;
            }
            fail(ERR_SYNTAX,"Unexpected Token in assignment statement",token.line_num,token.char_num);
            break;
        case type_brace_open:
            cmp_st();       
            break;
        case type_kw_int:
        case type_kw_double:
        case type_kw_string:
        case type_kw_auto:
            var_dd(); 
            if(token.type == type_semicolon)
            {
                next();
                return;
            }
            fail(ERR_SYNTAX,"Unexpected Token in variable definition/declaration",token.line_num,token.char_num);
            break;
        case type_kw_cin:
            st_in();
            if(token.type == type_semicolon)
            {
                next();
                return;
            }
            fail(ERR_SYNTAX,"Unexpected Token in input statement",token.line_num,token.char_num);
            break;
        case type_kw_cout:
            st_out();
            if(token.type == type_semicolon)
            {
                next();
                return;
            }
            fail(ERR_SYNTAX,"Unexpected Token in output statement",token.line_num,token.char_num);
            break;
        case type_kw_if:
            st_if();
            break;
        case type_kw_for:
            st_for();
            break;
        case type_kw_while:
            st_while();
            break;
        case type_kw_do:
            st_dowhile();
            break;
        case type_kw_return:
            {
                LOG_DBG("Ret %d",token.type);
                symbol_ptr res = parse_expr(type_semicolon,0);
                cg_return(res);
                next();
                return;
            }
            fail(ERR_SYNTAX,"Unexpected Token in return statement",token.line_num,token.char_num);
            break;
        default:
            fail(ERR_SYNTAX,"Unexpected token in statement",token.line_num,token.char_num);
            break;
    }
}

void st_assign(token_type ending)
{
    string var_name;
    token_t eq;
    if(token.type == type_identifier)
    {
        var_name = tstr_duplicate(token.identifier);
        LOG_DBG("Assign %s",str_to_c_str(token.identifier));
        eq = next();
        symbol_ptr res = parse_expr(ending,0);
        cg_assign(var_name,res);
        tstr_free(var_name);
    }
}


void var_dd()
{  
    token_t var_type;
    token_t var_name;
    var_type = token;
    if(is_token_type(token) || token.type == type_kw_auto)
    {
        var_type = token;
        if(next_token_expect(var_name,type_identifier))
        {
            string id1 = tstr_duplicate(var_name.identifier);
            next();
            if(token.type == type_semicolon)
            {
                cg_var_declare(id1, var_type.type);
                if(var_type.type==type_kw_auto)
                {
                    fail(ERR_TYPE_UNKNOWN,"Could not determine type",token.line_num,token.char_num);
                }
            }
            else if(token.type == type_equal)
            {
                token_type type = var_type.type;
                symbol_ptr res = parse_expr(type_semicolon,0);

                if(type==type_kw_auto)
                    type=res->variable.type;
                cg_var_define(id1, type, res);
            }
            else
            {
                //err
                fail(ERR_SYNTAX,"Unexpected Token in variable definiton/declaration",token.line_num,token.char_num);
            }
        }
        else
            fail(ERR_SYNTAX, "Missing variable name",token.line_num,token.char_num);

    }
}

void st_in()
{
    token_t in_id;
    if(token.type == type_kw_cin)
    {
        if(next().type == type_stream_right)
        {
            if(next_token_expect(in_id,type_identifier))
            {

                LOG_DBG("Stream in %s",str_to_c_str(in_id.identifier));
                cg_input(in_id.identifier);
                next();
                st_in_l();
                return;
            }
        }
    }
    fail(ERR_SYNTAX,"Unexpected Token in input construct",token.line_num,token.char_num);
}
void st_in_l()
{
    if(token.type == type_stream_right)
    {
        token_t in_id = {0};
        if(next_token_expect(in_id,type_identifier))
        {
            LOG_DBG("+Stream in_l %s",str_to_c_str(in_id.identifier));
            cg_input(in_id.identifier);
            next(); 
            st_in_l();
            return;
        }
    }
    else if(token.type == type_semicolon)
    {

        LOG_DBG("+Stream in_l end",0);
        return; 
    }
    fail(ERR_SYNTAX,"Unexpected Token in input construct",token.line_num,token.char_num);
}

void st_out()
{
    if(token.type == type_kw_cout)
    {
        if(next().type == type_stream_left)
        {

            LOG_DBG("Stream out %d",0);  
            symbol_ptr res = parse_expr(type_stream_left,type_semicolon);
            //TODO, expression result
            cg_output(res);

            st_out_l();
            return;
        }
    }
    fail(ERR_SYNTAX,"Unexpected Token in output construct",token.line_num,token.char_num);
}
void st_out_l()
{
    
    if(token.type == type_stream_left)
    {

        //next();

        LOG_DBG("Stream out %d",0);  
        symbol_ptr res = parse_expr(type_stream_left,type_semicolon);
        //TODO, expression result
        cg_output(res);

        st_out_l();
        return;
    }
    else if(token.type == type_semicolon)
    {

        LOG_DBG("+Stream out_l end",0);
        return; 
    }
    fail(ERR_SYNTAX,"Unexpected Token in output construct",token.line_num,token.char_num);
}

void st_if()
{
    vec_instr* code_backup = NULL;
    vec_instr* pos_backup = NULL;

    if(token.type == type_kw_if)
    {
        if(next().type==type_bracket_open)
        {
            symbol_ptr cond = parse_expr(type_bracket_close,0);
            if(cond)
            {
                code_backup = code_buffer;
                code_buffer = vec_instr_init();
            }
            next(); // )
            st();

            LOG_DBG("Conditional code : ", NULL);
            vec_instr_foreach_ptr(code_backup, instr_debug);

            pos_backup = code_buffer;
            int lenPos = vec_instr_size(pos_backup);
            int lenNeg = 0;
            if(token.type == type_kw_else)
            {
                code_buffer = vec_instr_init();
                LOG_DBG("If-else",0);
                next();
                st();
                //current code buffer is negative branch
                lenNeg = vec_instr_size(code_buffer);
                if(lenNeg > 0)
                {
                    instr jmp;
                    jmp.type = INSTR_JMP;
                    jmp.offset1 = lenNeg;//jump over negative branch
                    jmp.second = NULL;
                    jmp.third = NULL;
                    vec_instr_push(pos_backup,jmp);
                    lenPos+=1;
                }

                LOG_DBG("After adding jump code : ", NULL);
                vec_instr_foreach_ptr(pos_backup, instr_debug);
                LOG_DBG("After adding jump code : ", NULL);
                vec_instr_foreach_ptr(code_buffer, instr_debug);

                // Append negative branch to positive, branch lengths are saved
                vec_instr_append(pos_backup,code_buffer);
                vec_instr_free(code_buffer);

                LOG_DBG("After adding jump code 2 : ", NULL);
                vec_instr_foreach_ptr(pos_backup, instr_debug);
            }
            instr jmp;
            jmp.type = INSTR_JMPC;
            jmp.op1 = cond;//Normal if jump
            jmp.offset2 = 0;
            jmp.offset3 = lenPos;
            vec_instr_push(code_backup,jmp);

            vec_instr_append(code_backup,pos_backup);
            vec_instr_free(pos_backup);

            LOG_DBG("After adding jump code 3 : ", NULL);
            vec_instr_foreach_ptr(code_backup, instr_debug);

            code_buffer = code_backup;

            return;
        }
    }
    fail(ERR_SYNTAX,"Unexpected Token in if/else construct",token.line_num,token.char_num);
}

void st_for()
{
    vec_instr* code_backup = NULL;
    vec_instr* cond_code = NULL;
    symbol_ptr cond = NULL;
    vec_instr* incr = NULL;
    if(token.type == type_kw_for)
    {
        cg_scope_enter();
        if(next().type == type_bracket_open)
        {
            next();
            LOG_DBG("For-1",0);
            var_dd();

            vec_instr_push(code_buffer,instr_gen(INSTR_NOP,NULL,NULL,NULL));

            LOG_DBG("After adding init : ", NULL);
            vec_instr_foreach_ptr(code_buffer, instr_debug);

            code_backup = code_buffer;
            code_buffer = vec_instr_init();

            if(token.type==type_semicolon)
            {   
                //next();


                cond = parse_expr(type_semicolon,type_semicolon);
                next();

                LOG_DBG("After adding comparation part : ", NULL);
                vec_instr_foreach_ptr(code_buffer, instr_debug);

                cond_code = code_buffer;
                code_buffer = vec_instr_init();


                st_assign(type_bracket_close);

                LOG_DBG("After adding increment part : ", NULL);
                vec_instr_foreach_ptr(code_buffer, instr_debug);

                incr = code_buffer;
                code_buffer = vec_instr_init();



                next();
                st();


                // code backup - previous code + variable declaration
                // cond_code - condition code, without checks for now
                // cond - condition variable to check
                // incr - increment code
                // code_buffer - statement code

                // append increment to statement code
                vec_instr_append(code_buffer,incr);
                vec_instr_free(incr);

                //Create jump instruction - condtition chekc
                instr jmpc;
                jmpc.type = INSTR_JMPC;
                jmpc.op1 = cond;
                jmpc.offset2 = 0; // if pos, just continue
                jmpc.offset3 = vec_instr_size(code_buffer) + 1; // inner block code + increment code + 1 representing jump back to the beginning
                vec_instr_push(cond_code,jmpc);


                instr jmp;
                jmp.type = INSTR_JMP;
                /* jumping up - (inner code+increment + this instruction) + condition code +1 because we are jumping up */
                jmp.op1 = -( vec_instr_size(code_buffer) + vec_instr_size(cond_code) +2);
                jmp.offset2 = 0;
                jmp.offset3 = 0;
                vec_instr_push(code_buffer,jmp);

                vec_instr_append(code_backup,cond_code);
                vec_instr_append(code_backup,code_buffer);


                vec_instr_free(cond_code);

                vec_instr_free(code_buffer);
                code_buffer = code_backup;

                cg_scope_exit();
                return;
            }
        }
    }   
    fail(ERR_SYNTAX,"Unexpected Token in 'for' construct",token.line_num,token.char_num);
}

void st_dowhile()
{
    vec_instr* backup = code_buffer;
    code_buffer = vec_instr_init();
    int length = 0;
    int cond_length = 0;
    symbol_ptr cond = NULL;
    if(token.type == type_kw_do)
    {
        next();
        st();
        length = vec_instr_size(code_buffer); // inner statement code
        vec_instr_append(backup,code_buffer);
        vec_instr_clear(code_buffer);
        if(token.type == type_kw_while)
        {
            next();// (
            cond = parse_expr(type_bracket_close,0);
            cond_length = vec_instr_size(code_buffer);
            instr jmp;
            jmp.type = INSTR_JMPC;
            jmp.first = cond;
            jmp.offset2 = -(length+cond_length+1);
            jmp.offset3 = 0;
            vec_instr_append(backup,code_buffer);
            vec_instr_push(backup,jmp);
            vec_instr_free(code_buffer);
            code_buffer = backup;
            next(); //consume closing bracket, stay on semicolon
            if(token.type == type_semicolon)
            {
                next();
                return;
            }
        }
   
    }
   
    fail(ERR_SYNTAX,"Unexpected Token in 'do/while' construct1",token.line_num,token.char_num);

}
void st_while()
{
    vec_instr * backup = code_buffer;
    code_buffer = vec_instr_init();
    vec_instr *cond_code = NULL;
    symbol_ptr cond = NULL;
    int cond_length = 0;
    int st_length = 0;

    if(token.type == type_kw_while)
    {
        next();// (
        cond = parse_expr(type_bracket_close,0);
        cond_code = code_buffer;
        code_buffer = vec_instr_init();
        next();// )
        st();

        cond_length = vec_instr_size(cond_code);
        st_length = vec_instr_size(code_buffer);

        instr cond_jmp;
        cond_jmp.type = INSTR_JMPC;
        cond_jmp.op1 = cond;
        cond_jmp.offset2 = 0;
        cond_jmp.offset3 = st_length+1;
        vec_instr_push(cond_code,cond_jmp);

        instr jmp;
        jmp.type=INSTR_JMP;
        jmp.offset1 = - (st_length+cond_length+2);
        vec_instr_push(code_buffer,jmp);

        vec_instr_append(backup,cond_code);
        vec_instr_append(backup,code_buffer);
        vec_instr_free(cond_code);
        vec_instr_free(code_buffer);
        code_buffer=backup;

        return;
    } 
    fail(ERR_SYNTAX,"Unexpected Token in 'while' construct",token.line_num,token.char_num);


}
void parse()
{
    program();
    LOG_DBG("END %d",token.type);
}

void cg_init()
{
    scope = tstr_init();
}
//Called when encountering new function signature 
void cg_new_fun(token_type type, string name)
{
    tstr_push(scope ,'@');
    tstr_append(scope,name);
    current_fun = sym_gen_func();

    current_fun->function.name = name;
    current_fun->function.defined = false;
    current_fun->function.ret_type = type;
    current_fun->function.stack_size = 0;
}

//Called when encountering new function param
void cg_fun_param(token_type type, string name)
{
    symbol_ptr param = sym_gen_var();
    param->variable.type = type;
    param->variable.mem_offset = current_fun->function.stack_size;
    current_fun->function.stack_size += 1;

    
    /* We cant put this symbol in lookup table, we don't know whether the function signature is valid */
    param->variable.name =tstr_init();
    tstr_append(param->variable.name,name);

    vec_symbol_ptr_push(current_fun->function.params,param);
}
/*  Called when function is defined, function should check for previous declarations,
    compare signatures , and all further code generation functions should modify this
    function's data (generation of temp variables, appending instructions )*/
void cg_fun_defined()
{
    symbol_ptr old_fun = lookup_fun(current_fun->function.name);
    if(old_fun)
    {
        if(old_fun->function.defined || old_fun->function.internal)
        {
            //Multiple definitions
            fail(ERR_SEMANTIC,"Multiple function definitions",token.line_num,token.char_num);
            return;
        }
        else if(function_signatures_equal(current_fun,old_fun))
        {
            //Everything ok , we drop current function and work with old one
            for (int i = 0; i < vec_symbol_ptr_size(current_fun->function.params); ++i)
            {
                symbol_ptr param = current_fun->function.params->data[i];
                tstr_free(param->variable.name);
                free(param);
            }
            vec_symbol_ptr_free(current_fun->function.params);
            vec_instr_free(current_fun->function.code); // No code has been generated so far
            tstr_free(current_fun->function.name);
            free(current_fun);
            current_fun = old_fun;
        }
        else
            fail(ERR_SEMANTIC, "Function redefined/redeclared with wrong signature", token.line_num, token.char_num);

    }
    else
    {
        insert_func_sym(current_fun->function.name,current_fun);
        for (int i = 0; i < current_fun->function.params->size; ++i)
        {
            symbol_ptr param = current_fun->function.params->data[i];
            insert_var_sym(param->variable.name,scope,param);
        }
    }
    current_fun->function.defined = true;
    code_buffer= current_fun->function.code;
    //Everything resolved here

}
/*  Called when function is declared, 
    this function should check for previous declarations of function with same names */
void cg_fun_declared()
{
    symbol_ptr old_fun = lookup_fun(current_fun->function.name);
    if(old_fun)
    {
        if(function_signatures_equal(current_fun,old_fun))
        {
            //Everything ok , we drop current function and work with old one
            for (int i = 0; i < vec_symbol_ptr_size(current_fun->function.params); ++i)
            {
                symbol_ptr param = current_fun->function.params->data[i];
                tstr_free(param->variable.name);
                free(param);
                /* code */
            }
            vec_symbol_ptr_free(current_fun->function.params);
            vec_instr_free(current_fun->function.code); // No code has been generated so far 
            tstr_free(current_fun->function.name);   
            free(current_fun);
            current_fun = old_fun;        
        }
        else
            fail(ERR_SEMANTIC, "Function redeclared/redefined with wrong function signature", token.line_num, token.char_num);
        /*
        {
            //Err multiple functions with same name
            LOG_ERR("Multiuple functions-Same name",0);
        }
         */
            
        
    }
    else
    {
        insert_func_sym(current_fun->function.name,current_fun);
        for (int i = 0; i < current_fun->function.params->size; ++i)
        {
            symbol_ptr param = current_fun->function.params->data[i];
            insert_var_sym(param->variable.name,scope,param);
            /* code */
        }
    }
    code_buffer= current_fun->function.code;
    //Aditional work can be done here
}

/*  Called when function definition/declaration has ended, function should save all
    necessary info and clean temp variables*/
void cg_fun_ended()
{
    current_fun= NULL;
    code_buffer = NULL;
    tstr_clear(scope);
}

void cg_scope_enter()
{
    static int counter = 0;
    int len = 2 + counter%10; //1 characters, decimal number and terminating 0
    LOG_DBG("Don't mind me, just making some space %d\n", len);
    tstr_make_space(scope,len);
    scope->size += sprintf(scope->data + scope->size ,".%d",counter);
    LOG_DBG("Pushed scope %s",scope->data);
    counter++;
}
/*  Called when exiting scope, function should remove last part from full scope name */
void cg_scope_exit()
{
    str_delete_downto(scope,'.');
}
void cg_assign(string name,symbol_ptr res)
{
    if(res)
    {
        symbol_ptr var = lookup_var_global(name, scope);
        LOG_DBG("Assignees name is : \"%s\"\n", str_to_c_str(name));
        if(var)
        {
            symbol_ptr result = type_match(var->variable.type,res,code_buffer);

            vec_instr_push(code_buffer,instr_gen(INSTR_MOV,var,result,NULL)); 
            return;
        }
        fail(ERR_SEMANTIC,"Unknown Variable",token.line_num,token.char_num);
    }
    fail(ERR_INTERNAL,"Null expression",token.line_num,token.char_num);
}

void cg_var_define(string name,token_type type ,symbol_ptr res)
{
    symbol_ptr sym = lookup_var_local(name,scope);
    if(!sym)
    {
        /*  Generate varialbe symbol, append expression code
            and move result of expression into this symbol */
        //sym = cg_gen_temp_var();
        sym = sym_gen_var();
        sym->variable.type = type;
        sym->variable.name = name;
        sym->variable.mem_offset = current_fun->function.stack_size++;
        insert_var_sym(name,scope,sym);

        cg_assign(name,res);
        return;
    }
    //error, multiple definitions of variable
    fail(ERR_SEMANTIC,"Variable redefinition",token.line_num,token.char_num);
}

void cg_var_declare(string name,token_type type)
{
    symbol_ptr sym = lookup_var_local(name,scope);

    /* Also check, if a function with the same name doesn't exist */
    /*
    if(!sym)
        sym = lookup_fun(name);
    */

    if(!sym)
    {
        //sym = cg_gen_temp_var();
        sym = sym_gen_var();
        sym->variable.type = type;
        sym->variable.name = name;
        sym->variable.mem_offset = current_fun->function.stack_size++;
        insert_var_sym(name,scope,sym);

        /* We need to prepare the variable before use */
        vec_instr_push(code_buffer,instr_gen(INSTR_PREP_VAR,sym,NULL,NULL));

        return;
    }
    fail(ERR_SEMANTIC,"Variable redeclaration",token.line_num,token.char_num);
}

void cg_output(symbol_ptr res)
{
    if(res)
    {
        vec_instr_push(code_buffer,instr_gen(INSTR_OUT,res,NULL,NULL));
        return;
    }
    fail(ERR_INTERNAL,"Null expression",token.line_num,token.char_num);
}
void cg_input(string name)
{
    symbol_ptr var = lookup_var_global(name,scope); 
    if(var)
    {
        vec_instr_push(code_buffer,instr_gen(INSTR_IN,var,NULL,NULL)); 
        return;
    }
    fail(ERR_SEMANTIC,"Unknown variable",token.line_num,token.char_num);
}


void cg_return(symbol_ptr res)
{
    //Just append expression code to buffer and add return instruction
    if(res)
    {
        symbol_ptr src = type_match(current_fun->function.ret_type,res,code_buffer);
        vec_instr_push(code_buffer,instr_gen(INSTR_RET,src,NULL,NULL));
        return;
    }
    fail(ERR_INTERNAL,"Null expression",token.line_num,token.char_num);
}

/*
 * Generate a new temporary variable and return the pointer to it.
 *
 * @return Returns new temporary variable inside current function.
 */
symbol_ptr cg_gen_temp_var()
{
    symbol_ptr tempVar = sym_gen_temp();
    tempVar->type = sym_var;
    tempVar->variable.name = NULL;
    tempVar->variable.mem_offset = current_fun->function.stack_size++;
    return tempVar;
}

/*
 * Search for a symbol in current and enveloping scopes.
 *
 * @param identifier Name of the symbol.
 * @return Returns pointer to a symbol in current or enveloping scope, or NULL, if no symbol with given ID exists.
 */
symbol_ptr cg_get_var(string identifier)
{
    symbol_ptr found = NULL;
    found = lookup_var_global(identifier, scope);
    return found;
}

/*
 * Generate arithmetic instruction of given type, using the operands supplied.
 *
 * @param res Where to store result.
 * @param op1 First operand.
 * @param op2 Second opearnd.
 * @param op Operation, for example== - "type_plus".
 * @param buffer Where to add the instruction.
 * @return Returns the symbol containing result.
 */
symbol_ptr cg_gen_ar_instr(symbol_ptr res, symbol_ptr op1, symbol_ptr op2, token_type op, vec_instr* buffer)
{
    LOG_DBG("Generating instruction for %s", token_type_name(op));
    switch(op)
    {
        case type_plus:
            vec_instr_push(buffer, instr_gen(INSTR_ADD, res, op1, op2));
            break;
        case type_minus:
            vec_instr_push(buffer, instr_gen(INSTR_SUB, res, op1, op2));
            break;
        case type_star:
            vec_instr_push(buffer, instr_gen(INSTR_MUL, res, op1, op2));
            break;
        case type_slash:
            vec_instr_push(buffer, instr_gen(INSTR_DIV, res, op1, op2));
            break;
        case type_dequal:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_EQ, res, op1, op2));
            break;
        case type_nequal:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_NEQ, res, op1, op2));
            break;
        case type_lequal:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_LEQ, res, op1, op2));
            break;
        case type_gequal:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_GEQ, res, op1, op2));
            break;
        case type_greater:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_GT, res, op1, op2));
            break;
        case type_lesser:
            vec_instr_push(buffer, instr_gen(INSTR_CMP_LT, res, op1, op2));
            break;
        default:
            fail(ERR_SYNTAX, "Expression parsing failed - unknown operation", token.line_num, token.char_num);
    }
    return res;
}

symbol_ptr type_match(token_type dest_type, symbol_ptr src,vec_instr* buffer)
{
    if(src->variable.type == dest_type)
    {
        return src;
    }
    else if(dest_type == type_kw_double && src->variable.type == type_kw_int)
    {
        symbol_ptr tmp= cg_gen_temp_var();
        tmp->variable.type = type_kw_double;
        vec_instr_push(buffer,instr_gen(INSTR_CVTID,tmp,src,NULL));
        return tmp;
    }
    else if(dest_type == type_kw_int && src->variable.type == type_kw_double)
    {
        symbol_ptr tmp= cg_gen_temp_var();
        tmp->variable.type = type_kw_int;
        vec_instr_push(buffer,instr_gen(INSTR_CVTDI,tmp,src,NULL));
        return tmp;
    }
    LOG_DBG("%s -> %s\n", token_type_name(src->variable.type), token_type_name(dest_type));
    fail(ERR_TYPE_MISMATCH,"No suitable type conversion",token.line_num,token.char_num);
    return NULL;

}

/*
 * Get the type of result of arithmetic operation with given operands.
 *
 * @param op1 First operand.
 * @param op2 Second operand.
 * @return Returns resulting type. If unable to resolve the type, returns type_eof.
 */
token_type get_res_type(symbol_ptr op1, symbol_ptr op2)
{
    /* Needs to be revamped */
    int type1, type2;
    if(op1->type == sym_var)
        type1 = op1->variable.type;
    else
        type1 = op1->constant.type;
    if(op2->type == sym_var)
        type2 = op2->variable.type;
    else
        type2 = op2->constant.type;

    LOG_DBG("Operating on types %s %s", token_type_name(type1), token_type_name(type2));

    if(type1 == type_kw_double && type2 == type_kw_int)
        return type_kw_double;
    if(type2 == type_kw_double && type1 == type_kw_int)
        return type_kw_double;
    if(type2 == type_kw_double && type1 == type_kw_double)
        return type_kw_double;
    if(type1 == type_kw_int && type2 == type_kw_int)
        return type_kw_int;
    if(type1 == type_kw_string && type2 == type_kw_string)
        return type_kw_string;

    return type_eof;
}

/*
 * Free resources used by code generation
 */
void cg_free()
{
    tstr_free(scope);
}
