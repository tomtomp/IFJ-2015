#ifndef SCANNER_H
#define SCANNER_H
#include <errno.h>
#include "main.h"
#include "token.h"


/*  Function for initializing the scanner and opening provided file
    returns true if file was opened succesfully
*/
bool scanner_init(const char* filename);

/*
 * Free resources used by scanner
 */
void scanner_free();

/*  Returns next token from the stream, if there is an error, 
    pushes error on the error stack and tries to continue
*/
token_t get_token();

#endif