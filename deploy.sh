#/bin/sh

ZIPNAME=xpokor67.tgz

rm -rf ./deploy/

rm $ZIPNAME

mkdir deploy

cp *.c ./deploy/
cp *.ct ./deploy/
cp *.h ./deploy/
cp *.ht ./deploy/
cp Makefile deploy/Makefile
cp ./docs/dokumentace.pdf ./deploy/

echo "xpokor67:20" >> deploy/rozdeleni
echo "xhorni14:20" >> deploy/rozdeleni
echo "xpolas34:20" >> deploy/rozdeleni
echo "xsladk07:20" >> deploy/rozdeleni
echo "xrevaj00:20" >> deploy/rozdeleni

echo "WHILE" >> deploy/rozsireni
echo "FUNEXP" >> deploy/rozsireni
echo "SIMPLE" >> deploy/rozsireni

cd ./deploy/

tar -cvzf ../${ZIPNAME} ./*
