#include "frame.h"

#define VEC_DATA_TYPE frame_ptr
#define VEC_DEFAULT_VAL {0}
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

/*
 * Allocates and initializes a new frame. Local stack will be created according to the size given in function symbol.
 *
 * @param function Function symbol pointer, used for getting information about the frame.
 * @param ret_sym Return symbol, used for a pointer to preceding frame, can be NULL and passed in later.
 * @return Returns malloced frame, or NULL, if the function failed.
 */
frame_ptr fr_new(symbol_ptr function, symbol_ptr ret_sym)
{
    LOG_DBG("Creating new frame : size = %d", function->function.stack_size);

    frame_ptr newFrame = malloc(sizeof(frame));

    CHK_ALLOC(newFrame);

    newFrame->function = function;
    newFrame->instr_counter = 0;
    newFrame->return_sym = ret_sym;
    newFrame->local_stack = calloc(function->function.stack_size, sizeof(mem_item));
    //memset(newFrame->local_stack, 0, sizeof(mem_item) * function->function.stack_size);

    CHK_ALLOC(newFrame->local_stack);

    return newFrame;
}

/*
 * Print debug information about given frame.
 *
 * @param fr Frame to process
 */
void fr_debug(frame_ptr fr)
{
#ifdef DEBUG
    printf("/=======FRAME_DBG========\\\n");
    printf("Function : %p\n", fr->function);
    printf("Instr_cntr : %d\n", fr->instr_counter);
    if(fr->function)
        printf("Local stack size : %d\n", fr->function->function.stack_size);
    printf("Ret symbol : %p\n", fr->return_sym);
    printf("|=======FUNC_INFO========|\n");
    sym_debug(fr->function);
    printf("|=======RET_SYM_INFO=====|\n");
    sym_debug(fr->return_sym);
    printf("\\========================/\n");
#endif
}
