#ifndef INSTR_H
#define INSTR_H

#include <stdio.h>
#include <stdlib.h>

typedef enum 
{
    INSTR_NOP = 0,

    INSTR_MOV, /* op1 = op2 */
    INSTR_OUT, /* op1 -> stdout */
    INSTR_IN, /* stdin -> op1 */

    INSTR_CVTID, // op1 = (double)op2 , op2 must be int
    INSTR_CVTDI, // op1 = (int)op2 , op2 must be

    INSTR_RET, /* return op1 */
    INSTR_FR_PREP, /* Prepare new frame for function in op1 */
    INSTR_FR_PARAM, /* Copy op1 into the top most prep frame to the position given by offset2 */
    INSTR_FR_CALL, /* Calls the currently top most prep frame, op1 contains the return symbol on the last frame */

    INSTR_PREP_VAR, /* Prepare variable for use, called each time a declaration is in source code */
    /*
     * Needs to be done, for example :
     * int b = 1;
     * for(int i = 0; i < 5; i++)
     * {
     *   int a;
     *   if(i == 0) <- The variable is defined only in the first pass, and is undefined in the rest
     *     a = 1;
     *   b = a + b;
     * }
     */
    
    INSTR_JMP, // IC += offset1
    INSTR_JMPC,// if(op1) IC+= offset2 else IC+= offset3  (IC = instruction counter)
    INSTR_CMP_EQ, // op1 = op2 == op3 ? 1:0
    INSTR_CMP_NEQ, // op1 = op2 != op3 ? 1:0
    INSTR_CMP_GT ,// op1 = op2 > op3 ? 1: 0
    INSTR_CMP_GEQ ,//op1 = op2 >= op3 ? 1: 0

    INSTR_CMP_LT ,// op1 = op2 < op3 ? 1: 0
    INSTR_CMP_LEQ ,//op1 = op2 <= op3 ? 1: 0

    INSTR_AND, // op1 = op2 & op3
    INSTR_OR,
    INSTR_NOT,
    INSTR_XOR,
    
    /* Arithmetic instructions */
    INSTR_ADD, // op1 = op2+op3
    INSTR_SUB,
    INSTR_MUL,
    INSTR_DIV,
    /* Jump instructions */

    /* Please add every new instruction to the INSTR_NAMES, in the .c file */
    INSTR_DBG,//Debug marker , not interpreted

    INSTR_FUN_END, /* Function end symbol, if reached error occurs */

    INSTR_LAST
} instr_type;

extern const char * INSTR_NAMES[];

/*
 * Gets the instruction name from given code.
 *
 * @param code Code to interpret
 * @return Returns pointer to a c-style string, which contains the name associated with given instruction code.
 */
static inline const char* instr_get_name(int code)
{
    return (code < INSTR_LAST && code >= 0) ? INSTR_NAMES[code] : "!!UNKNOWN INSTRUCTION!!";
}

typedef struct TSymbolData* symbol_ptr;
typedef struct TInstr
{
    instr_type type;
    union
    {
        symbol_ptr first;
        symbol_ptr op1;
        int offset1;
    };
    union
    {
        symbol_ptr second;
        symbol_ptr op2;
        int offset2;
    };
    union
    {
        symbol_ptr third;
        symbol_ptr op3;
        int offset3;
    };
} instr;

typedef instr* instr_ptr;


#define VEC_DATA_TYPE instr
#define VEC_DEFAULT_VAL {0}
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

/*
 * Print debug information about given instruction.
 *
 * @param instr Instruction to print info about
 */
void instr_debug(instr_ptr instr);

/*
 * Generate new instruction with the given attributes
 *
 * @param type Instruction type code
 * @param first First address
 * @param second Second address
 * @param third Third address
 * @return Returns the instruction
 */
static inline instr instr_gen(instr_type type, symbol_ptr first, symbol_ptr second, symbol_ptr third)
{
    instr newInstr = {.type = type, .first = first, .second = second, .third = third};
    return newInstr;
}

#endif
