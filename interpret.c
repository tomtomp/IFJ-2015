#include <limits.h>
#include <float.h>
#include "interpret.h"

/*
 * Free all strings in the global_free_later list, until the first dummy string (NULL).
 */
/*
void inter_do_free_later()
{
    string next = sList_pop(global_free_later);

    while(next)
    {
        tstr_free(next);
        next = sList_pop(global_free_later);
    }
}
 */

/* Internal function callers */

/* int length(string s), string substr(string s, int i, int n), string concat(string s1, string s2),
 * int find(string s, string search), string sort(string s) */

/*
 * Caller for internal function with signature :
 *   int length(string s);
 */
void caller_length()
{
    frame_ptr myFrame = fr_get_top();
    fr_pop();
    frame_ptr lastFrame = fr_get_top();

    symbol_ptr retSym = myFrame->return_sym;
    /* The first parameter "s", is on the first position */
    mem_item p_s = myFrame->local_stack[0];
    if(p_s.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function length", 0, 0);

    /* Calculate the length, cannot be longer, than max value of integer */
    int sLength = (int)strlen(str_to_c_str(p_s.str_val));

    /* Return the value */
    lastFrame->local_stack[retSym->variable.mem_offset].int_val = sLength;
    lastFrame->local_stack[retSym->variable.mem_offset].defined = true;

    //tstr_free(p_s.str_val);
    fr_free(myFrame);
}

/*
 * Caller for internal function with signature :
 * string substr(string s, int i, int n)
 */
void caller_substr()
{
    frame_ptr myFrame = fr_get_top();
    fr_pop();
    frame_ptr lastFrame = fr_get_top();

    symbol_ptr retSym = myFrame->return_sym;
    /* The first parameter "s", is on the first position */
    mem_item p_s = myFrame->local_stack[0];
    if(p_s.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);
    /* The second parameter "i", is on the second position */
    mem_item p_i = myFrame->local_stack[1];
    if(p_i.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);
    /* The third parameter "n", is on the third position */
    mem_item p_n = myFrame->local_stack[2];
    if(p_n.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);

    if(tstr_size(p_s.str_val) < p_i.int_val)
        fail(ERR_RUNTIME_OTHER, "starting substring index cannot be higher than string size", 0, 0);

    string newStr = str_substr(p_s.str_val, p_i.int_val, p_n.int_val);

    /* Return the value */
    mem_item *ret = &lastFrame->local_stack[retSym->variable.mem_offset];
    if(ret->defined && ret->isStr && !ret->isLazy && ret->str_val)
        //sList_push(global_free_later, ret->str_val);
        tstr_free(ret->str_val);
    ret->str_val = newStr;
    ret->isLazy = false;
    ret->defined = true;
    ret->isStr = true;

    //tstr_free(p_s.str_val);
    fr_free(myFrame);
}

/*
 * Caller for internal function with signature :
 * string concat(string s1, string s2)
 */
void caller_concat()
{
    frame_ptr myFrame = fr_get_top();
    fr_pop();
    frame_ptr lastFrame = fr_get_top();

    symbol_ptr retSym = myFrame->return_sym;
    /* The first parameter "s1", is on the first position */
    mem_item p_s1 = myFrame->local_stack[0];
    if(p_s1.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);
    /* The second parameter "s2", is on the second position */
    mem_item p_s2 = myFrame->local_stack[1];
    if(p_s2.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);

    string newStr = str_concat(p_s1.str_val, p_s2.str_val);

    /* Return the value */
    mem_item *ret = &lastFrame->local_stack[retSym->variable.mem_offset];
    if(ret->defined && ret->isStr && !ret->isLazy && ret->str_val)
        //sList_push(global_free_later, ret->str_val);
        tstr_free(ret->str_val);
    ret->str_val = newStr;
    ret->isLazy = false;
    ret->defined = true;
    ret->isStr = true;

    //tstr_free(p_s1.str_val);
    //tstr_free(p_s2.str_val);
    fr_free(myFrame);
}

/*
 * Caller for internal function with signature :
 * string sort(string s)
 */
void caller_sort()
{
    frame_ptr myFrame = fr_get_top();
    fr_pop();
    frame_ptr lastFrame = fr_get_top();

    symbol_ptr retSym = myFrame->return_sym;
    /* The first parameter "s", is on the first position */
    mem_item p_s = myFrame->local_stack[0];
    if(p_s.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);

    string newStr = tstr_duplicate(p_s.str_val);
    str_shell_sort(newStr);

    /* Return the value */
    mem_item *ret = &lastFrame->local_stack[retSym->variable.mem_offset];
    if(ret->defined && ret->isStr && !ret->isLazy && ret->str_val)
        //sList_push(global_free_later, ret->str_val);
        tstr_free(ret->str_val);
    ret->str_val = newStr;
    ret->isLazy = false;
    ret->defined = true;
    ret->isStr = true;

    //tstr_free(p_s.str_val);
    fr_free(myFrame);
}

/*
 * Caller for internal function with signature :
 * int find(string s, string search)
 */
void caller_find()
{
    frame_ptr myFrame = fr_get_top();
    fr_pop();
    frame_ptr lastFrame = fr_get_top();

    symbol_ptr retSym = myFrame->return_sym;
    /* The first parameter "s", is on the first position */
    mem_item p_s = myFrame->local_stack[0];
    if(p_s.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);
    /* The second parameter "search, is on the second position */
    mem_item p_search = myFrame->local_stack[1];
    if(p_search.defined == false)
        fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in internal function substr", 0, 0);

    int64_t index = kmp_search(p_s.str_val, p_search.str_val);

    /* Return the value */
    lastFrame->local_stack[retSym->variable.mem_offset].int_val = (int)index;
    lastFrame->local_stack[retSym->variable.mem_offset].defined = true;

    //tstr_free(p_s.str_val);
    //tstr_free(p_search.str_val);
    fr_free(myFrame);

}

/*
 * Runs pre-interpretation checks and initializes the interpreter.
 */
void interpret_init()
{
    /* Check, if all declared functions were defined */
    st_foreach(global_sym_table, sym_chk_fun);

    string startFunName = c_str_to_str("main");
    symbol_ptr startFunSym = lookup_fun(startFunName);

    if(startFunSym == NULL)
        fail(ERR_SEMANTIC, "Interpretation cannot begin - main function not declared", 0, 0);

    // Create the first frame for function main
    frame_ptr mainFrame = fr_new(startFunSym, NULL);
    fr_push(mainFrame);

    // Push the stopping element for main frame
    //sList_push(global_free_later, NULL);

    tstr_free(startFunName);
}

/*
 * Get instruction from given instruction vector and position.
 *
 * @param instrs Instruction vector.
 * @param pos Position of required instruction.
 * @return Returns the instruction.
 */
static inline instr inter_get_instr(vec_instr* instrs, int pos)
{
    //return *vec_instr_at(instrs, pos);
    return instrs->data[pos];
}

/*
 * Get int from input
 *
 * @return Returns integer received from stdin.
 */
int inter_in_int()
{
    int retVal = 0;
    bool running = true;
    int inputChar = 0;
    string input = tstr_init();
    enum {
        INT_first,
        INT_ok /* At least one number has been inputted */
    } stage;
    stage = INT_first;

    while(running)
    {
        inputChar = getchar();

        switch(stage)
        {
            case INT_first:
                if(is_digit(inputChar))
                {
                    tstr_push(input, (char)inputChar);
                    stage = INT_ok;
                }
                else if(!isblank(inputChar) && inputChar != '\n')
                    fail(ERR_RUNTIME_INPUT, "Integer input failed", 0, 0);
                break;
            case INT_ok:
                if(is_digit(inputChar))
                    tstr_push(input, (char)inputChar);
                else if(inputChar == '.' || inputChar == 'e' || inputChar == 'E')
                    fail(ERR_RUNTIME_INPUT, "Integer input failed", 0, 0);
                else
                {
                    running = false;
                    ungetc(inputChar, stdin);
                    char* endPtr = NULL;
                    retVal = (int)strtol(str_to_c_str(input), &endPtr, 10);
                    if(*endPtr != '\0' || errno == ERANGE || retVal < 0) /* If the conversion failed */
                        fail(ERR_RUNTIME_INPUT, "Integer input failed", 0, 0);
                }
                break;
            default:
                fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
        }
    }

    tstr_free(input);
    return retVal;
}

/*
 * Get double from given string.
 *
 * @param s String to convert to double.
 * @return Returns the double value.
 */
static inline double str_to_double(string s)
{
    double retVal = 0.0;
    char* endPtr = NULL;
    retVal = (double)strtod(str_to_c_str(s), &endPtr);
    if(*endPtr != '\0' || errno == ERANGE || retVal < 0.0) /* If the conversion failed */
        fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
    return retVal;
}

/*
 * Get double from input
 *
 * @return Returns double received from stdin.
 */
double inter_in_double()
{
    double retVal = 0.0;
    bool running = true;
    int inputChar = 0;
    string input = tstr_init();
    enum {
        DBL_dec,
        DBL_dec_ok, /* At least one digit in dec part */
        DBL_fract,
        DBL_fract_ok, /* At least one digit in fraction part */
        DBL_exp,
        DBL_exp_sign, /* Sign found */
        DBL_exp_ok /* At least one digit in exponential part */
    } stage;
    stage = DBL_dec;

    while(running)
    {
        inputChar = getchar();
        switch(stage)
        {
            case DBL_dec:
                if(is_digit(inputChar))
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_dec_ok;
                }
                else if(!isblank(inputChar) && inputChar != '\n')
                    fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
                break;
            case DBL_dec_ok:
                if(is_digit(inputChar))
                    tstr_push(input, (char)inputChar);
                else if(inputChar == '.')
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_fract;
                }
                else if(inputChar == 'e' || inputChar == 'E')
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_exp;
                }
                else
                {
                    running = false;
                    ungetc(inputChar, stdin);
                    retVal = str_to_double(input);
                }
                break;
            case DBL_fract:
                if(is_digit(inputChar))
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_fract_ok;
                }
                else
                    fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
                break;
            case DBL_fract_ok:
                if(is_digit(inputChar))
                    tstr_push(input, (char)inputChar);
                else if(inputChar == 'e' || inputChar == 'E')
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_exp;
                }
                else
                {
                    running = false;
                    ungetc(inputChar, stdin);
                    retVal = str_to_double(input);
                }
                break;
            case DBL_exp:
                if(is_digit(inputChar))
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_exp_ok;
                }
                else if(inputChar == '-' || inputChar == '+')
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_exp_sign;
                }
                else
                    fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
                break;
            case DBL_exp_sign:
                if(is_digit(inputChar))
                {
                    tstr_push(input, (char)inputChar);
                    stage = DBL_exp_ok;
                }
                else
                    fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
                break;
            case DBL_exp_ok:
                if(is_digit(inputChar))
                    tstr_push(input, (char)inputChar);
                else
                {
                    running = false;
                    ungetc(inputChar, stdin);
                    retVal = str_to_double(input);
                }
                break;
            default:
                fail(ERR_RUNTIME_INPUT, "Double input failed", 0, 0);
        }
    }

    tstr_free(input);
    return retVal;
}

/*
 * Get string from input
 *
 * @return Returns string received from stdin.
 */
string inter_in_string()
{
    string retVal = tstr_init();

    int c = getchar();
    bool word = false;
    bool running = true;

    while(running)
    {
        if(!word)
        {
            if(!(isblank(c) || c == '\n'))
                word = true;
            else
                c = getchar();
        }
        else
        {
            if(isblank(c) || c == '\n')
                running = false;
            else
            {
                tstr_push(retVal, (char)c);
                c = getchar();
            }
        }
    }

    return retVal;
}

/*
 * Executes move operation :
 *   first = second
 *
 * @param first Offset of the first operand.
 * @param firstStack Where to look for for the first symbol.
 * @param second Second operand.
 * @param secondStack Where to look for for the second symbol.
 */
static inline void inter_mov(int first, mem_item* firstStack, symbol_ptr second, mem_item* secondStack)
{
    if(second->type == sym_const)
    {
        firstStack[first].defined = true;
        switch(second->constant.type)
        {
            case type_kw_int:
                firstStack[first].int_val = second->constant.int_val;
                LOG_DBG("Moving int literal %d", second->constant.int_val);
                break;
            case type_kw_double:
                firstStack[first].double_val = second->constant.double_val;
                LOG_DBG("Moving double literal %g", second->constant.double_val);
                break;
            case type_kw_string:
                /* Free the current content */
                if(firstStack[first].defined && firstStack[first].str_val != NULL)
                        tstr_free(firstStack[first].str_val);
                else
                    firstStack[first].isStr = true;
                firstStack[first].str_val = str_to_str(second->constant.string_val);
                LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                break;
            default:
                LOG_DBG("ERROR, unknown constant type!", NULL);
                firstStack[first].int_val = -1;
                break;
        }
    }
    else
    {
        if(secondStack[second->variable.mem_offset].defined == false)
            fail(ERR_UNINITALIZED_VAR, "Cannot move undefined variable", 0, 0);
        firstStack[first].defined = true;
        switch(second->variable.type)
        {
            case type_kw_int:
                firstStack[first].int_val = secondStack[second->variable.mem_offset].int_val;
                LOG_DBG("Moving int %d", secondStack[second->variable.mem_offset].int_val);
                break;
            case type_kw_double:
                firstStack[first].double_val = secondStack[second->variable.mem_offset].double_val;
                LOG_DBG("Moving double %g", secondStack[second->variable.mem_offset].double_val);
                break;
            case type_kw_string:
                /* Free the current content */
                if(firstStack[first].defined && firstStack[first].str_val != NULL)
                        tstr_free(firstStack[first].str_val);
                else
                    firstStack[first].isStr = true;
                firstStack[first].str_val = str_to_str(secondStack[second->variable.mem_offset].str_val);
                LOG_DBG("Moving string %s", str_to_c_str(secondStack[second->variable.mem_offset].str_val));
                break;
            default:
                LOG_DBG("ERROR, unknown variable type!", NULL);
                firstStack[first].int_val = -1;
                break;
        }
    }

}

/*
 * Check if given symbol is defined in given stack.
 *
 * @param sym Symbol to check.
 * @param stack Stack to search in.
 * @return Returns True, if the symbol is defined, else returns False.
 */
static inline bool inter_get_def(symbol_ptr sym, mem_item* stack)
{
    if(sym->type == sym_const)
        return true;
    else
        return stack[sym->variable.mem_offset].defined;
}

/*
 * Main interpreting routine.
 */
void interpret()
{
    //printf("==========================NOW INTERPRETING=============================\n");

    /* Select the top-most frame as current */
    frame_ptr curFrame = fr_get_top();
    vec_instr* curInstrs = curFrame->function->function.code;
    int* curPC = &curFrame->instr_counter;
    instr curInstr = inter_get_instr(curInstrs, *curPC);
    mem_item* curStack = curFrame->local_stack;
    symbol_ptr first, second, third;
    bool flag = false;
    mem_item *valOne, *valTwo, *valThree;
    string deleteLater = NULL;

    frame_ptr newFrame = NULL;
    vec_frame_ptr* prepFrames = vec_frame_ptr_init();

    bool running = true;

    while(running)
    {
        switch(curInstr.type)
        {
            case INSTR_NOP:
                LOG_DBG("NOP instruction running", NULL);

                (*curPC)++;

                break;
            case INSTR_MOV:
                LOG_DBG("MOV instruction running", NULL);
                first = curInstr.first;
                valOne = &curStack[first->variable.mem_offset];
                second = curInstr.second;

                //inter_mov(first->variable.mem_offset, curStack, second, curStack);

                LOG_DBG("first %p", first);
                LOG_DBG("second %p", second);

                valOne->defined = true;
                if(second->type == sym_const)
                {
                    switch(second->constant.type)
                    {
                        case type_kw_int:
                            valOne->int_val= second->constant.int_val;
#if 0
                            if(valOne->int_val < 0)
                                fail(ERR_RUNTIME_OTHER, "Error, integer cannot be negative.", 0, 0);
#endif
                            LOG_DBG("Moving int literal %d", second->constant.int_val);
                            break;
                        case type_kw_double:
                            valOne->double_val= second->constant.double_val;
#if 0
                            if(valOne->double_val < 0)
                                fail(ERR_RUNTIME_OTHER, "Error, double cannot be negative.", 0, 0);
#endif
                            LOG_DBG("Moving double literal %g", second->constant.double_val);
                            break;
                        case type_kw_string:
                            if(valOne->defined && !valOne->isLazy && valOne->str_val)
                                //sList_push(global_free_later, valOne->str_val);
                                tstr_free(valOne->str_val);

                            /* Lazy string init */
                            valOne->str_val = second->constant.string_val;
                            valOne->isStr = true;
                            valOne->isLazy = true;
                            LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                    }
                }
                else
                {
                    valTwo = &curStack[second->variable.mem_offset];

                    if(!valTwo->defined)
                        fail(ERR_UNINITALIZED_VAR, "Moving unitialized variable", 0, 0);

                    switch(second->variable.type)
                    {
                        case type_kw_int:
                            valOne->int_val= valTwo->int_val;
#if 0
                            if(valOne->int_val < 0)
                                fail(ERR_RUNTIME_OTHER, "Error, integer cannot be negative.", 0, 0);
#endif
                            LOG_DBG("Moving int variable %d", second->constant.int_val);
                            break;
                        case type_kw_double:
                            valOne->double_val= valTwo->double_val;
#if 0
                            if(valOne->double_val < 0)
                                fail(ERR_RUNTIME_OTHER, "Error, double cannot be negative.", 0, 0);
#endif
                            LOG_DBG("Moving double variable %g", second->constant.double_val);
                            break;
                        case type_kw_string:
                            if(valOne->defined && !valOne->isLazy && valOne->str_val)
                                //sList_push(global_free_later, valOne->str_val);
                                /* Save the deletion to later, in case we are assigning to the same variable */
                                deleteLater = valOne->str_val;
                            else
                                deleteLater = NULL;

                            valOne->str_val = str_to_str(valTwo->str_val);
                            valOne->isStr = true;
                            valOne->isLazy = false;
                            if(deleteLater)
                                tstr_free(deleteLater);
                            LOG_DBG("Moving string variable %s", str_to_c_str(second->constant.string_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                    }
                }

                (*curPC)++;

                break;
            case INSTR_OUT:
                LOG_DBG("OUT instruction running", NULL);
                first = curInstr.first;
                if(first->type == sym_const)
                {
                    switch(first->constant.type)
                    {
                        case type_kw_int:
                            printf("%d", first->constant.int_val);
                            LOG_DBG("Printing int literal %d", first->constant.int_val);
                            break;
                        case type_kw_double:
                            printf("%g", first->constant.double_val);
                            LOG_DBG("Printing double literal %d", first->constant.double_val);
                            break;
                        case type_kw_string:
                            printf("%s", str_to_c_str(first->constant.string_val));
                            LOG_DBG("Printing string literal %s", str_to_c_str(first->constant.string_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            break;
                    }
                }
                else
                {
                    if(curStack[first->variable.mem_offset].defined == false)
                        fail(ERR_UNINITALIZED_VAR, "Cannot print undefined variable", 0, 0);
                    switch(first->constant.type)
                    {
                        case type_kw_int:
                            printf("%d", curStack[first->variable.mem_offset].int_val);
                            LOG_DBG("Printing int literal %d", curStack[first->variable.mem_offset].int_val);
                            break;
                        case type_kw_double:
                            printf("%g", curStack[first->variable.mem_offset].double_val);
                            LOG_DBG("Printing double literal %g", curStack[first->variable.mem_offset].double_val);
                            break;
                        case type_kw_string:
                            printf("%s", str_to_c_str(curStack[first->variable.mem_offset].str_val));
                            LOG_DBG("Printing string literal %s", str_to_c_str(curStack[first->variable.mem_offset].str_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            break;
                    }
                }
#ifdef DEBUG
                putc('\n', stdout);
#endif

                (*curPC)++;

                break;
            case INSTR_IN:
                LOG_DBG("IN instruction running", NULL);

                first = curInstr.first;
                valOne = &curStack[first->variable.mem_offset];

                switch(first->variable.type)
                {
                    case type_kw_int:
                        valOne->int_val = inter_in_int();
                        break;
                    case type_kw_double:
                        valOne->double_val = inter_in_double();
                        break;
                    case type_kw_string:
                        if(valOne->defined && !valOne->isLazy && valOne->str_val)
                            //sList_push(global_free_later, valOne->str_val);
                            tstr_free(valOne->str_val);

                        valOne->str_val = inter_in_string();
                        valOne->isLazy = false;
                        valOne->isStr = true;
                        break;
                    default:
                        fail(ERR_RUNTIME_OTHER, "unknown type in cin", 0, 0);
                }

                curFrame->local_stack[first->variable.mem_offset].defined = true;

                (*curPC)++;

                break;
            case INSTR_CVTID:
                LOG_DBG("CVTID instruction running", NULL);

                first = curInstr.first;
                second = curInstr.second;

                if(second->type == sym_var)
                    curFrame->local_stack[first->variable.mem_offset].double_val =
                            (double)curFrame->local_stack[second->variable.mem_offset].int_val;
                else
                    curFrame->local_stack[first->variable.mem_offset].double_val = (double) second->constant.int_val;
                curFrame->local_stack[first->variable.mem_offset].defined = true;

                (*curPC)++;

                break;
            case INSTR_CVTDI:
                LOG_DBG("CVTDI instruction running", NULL);

                first = curInstr.first;
                second = curInstr.second;

                if(second->type == sym_var)
                {
                    valOne = &curFrame->local_stack[second->variable.mem_offset];
                    if(valOne->double_val > INT_MAX || valOne->double_val < INT_MIN)
                        fail(ERR_RUNTIME_OTHER, "Double too large/small to fit inside integer", 0, 0);
                    curFrame->local_stack[first->variable.mem_offset].int_val =
                            (int)valOne->double_val;
                }
                else
                {
                    if(second->constant.double_val > INT_MAX || second->constant.double_val < INT_MIN)
                        fail(ERR_RUNTIME_OTHER, "Double too large/small to fit inside integer", 0, 0);
                    curFrame->local_stack[first->variable.mem_offset].int_val = (int) second->constant.double_val;
                }
                curFrame->local_stack[first->variable.mem_offset].defined = true;

                (*curPC)++;

                break;
            case INSTR_RET:
                LOG_DBG("RET instruction running", NULL);
                if(vec_frame_ptr_size(global_frames) == 1)
                {
                    running = false;
                    fr_free(curFrame);
                    fr_pop();
                    //inter_do_free_later();
                }
                else
                {
                    frame_ptr lastFrame = *vec_frame_ptr_at_end(global_frames, 1);
                    string temp = NULL;
                    first = curFrame->return_sym;
                    valOne = &lastFrame->local_stack[first->variable.mem_offset];
                    second = curInstr.first;

                    //inter_mov(first->variable.mem_offset, lastFrame->local_stack, second, curStack);

                    valOne->defined = true;
                    if(second->type == sym_const)
                    {
                        switch(second->constant.type)
                        {
                            case type_kw_int:
                                valOne->int_val= second->constant.int_val;
                                LOG_DBG("Moving int literal %d", second->constant.int_val);
                                break;
                            case type_kw_double:
                                valOne->double_val= second->constant.double_val;
                                LOG_DBG("Moving double literal %d", second->constant.double_val);
                                break;
                            case type_kw_string:
                                if(valOne->defined && !valOne->isLazy && valOne->str_val)
                                    //sList_push(global_free_later, valOne->str_val);
                                    temp = valOne->str_val;

                                /* Lazy string init */
                                valOne->str_val = second->constant.string_val;
                                valOne->isLazy = true;
                                valOne->isStr = true;
                                LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                                break;
                            default:
                                LOG_DBG("ERROR, unknown constant type!", NULL);
                                fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                        }
                    }
                    else
                    {
                        valTwo = &curStack[second->variable.mem_offset];
                        switch(second->variable.type)
                        {
                            case type_kw_int:
                                valOne->int_val= valTwo->int_val;
                                LOG_DBG("Moving int literal %d", second->constant.int_val);
                                break;
                            case type_kw_double:
                                valOne->double_val= valTwo->int_val;
                                LOG_DBG("Moving double literal %d", second->constant.double_val);
                                break;
                            case type_kw_string:
                                if(valOne->defined && !valOne->isLazy && valOne->str_val)
                                    //sList_push(global_free_later, valOne->str_val);
                                    temp = valOne->str_val;

                                valOne->str_val = str_to_str(valTwo->str_val);
                                valOne->isLazy = false;
                                valOne->isStr = true;
                                LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                                break;
                            default:
                                LOG_DBG("ERROR, unknown constant type!", NULL);
                                fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                        }
                    }

                    fr_pop();
                    fr_free(curFrame);
                    //inter_do_free_later();

                    if(temp)
                        //sList_push(global_free_later, temp);
                        tstr_free(temp);

                    curFrame = *vec_frame_ptr_top(global_frames);
                    curInstrs = curFrame->function->function.code;
                    curPC = &curFrame->instr_counter;
                    curStack = curFrame->local_stack;
                }
                break;
            case INSTR_FR_PREP:
                LOG_DBG("FR_PREP instruction running", NULL);

                first = curInstr.first;

                newFrame = fr_new(first, NULL);

                vec_frame_ptr_push(prepFrames, newFrame);

                (*curPC)++;

                break;
            case INSTR_FR_PARAM:
                LOG_DBG("FR_PARAM instruction running", NULL);

                second = curInstr.first;
                newFrame = *vec_frame_ptr_top(prepFrames);
                valOne = &newFrame->local_stack[curInstr.offset2];

                //inter_mov(curInstr.offset2, newFrame->local_stack, second, curStack);

                valOne->defined = true;
                if(second->type == sym_const)
                {
                    switch(second->constant.type)
                    {
                        case type_kw_int:
                            valOne->int_val= second->constant.int_val;
                            LOG_DBG("Moving int literal %d", second->constant.int_val);
                            break;
                        case type_kw_double:
                            valOne->double_val= second->constant.double_val;
                            LOG_DBG("Moving double literal %d", second->constant.double_val);
                            break;
                        case type_kw_string:
                            /* Lazy string init */
                            valOne->str_val = second->constant.string_val;
                            valOne->isLazy = true;
                            valOne->isStr = true;
                            LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                    }
                }
                else
                {
                    valTwo = &curStack[second->variable.mem_offset];

                    if(!valTwo->defined)
                        fail(ERR_UNINITALIZED_VAR, "Passing unitialized variable as parameter", 0, 0);

                    switch(second->variable.type)
                    {
                        case type_kw_int:
                            valOne->int_val= valTwo->int_val;
                            LOG_DBG("Moving int literal %d", second->constant.int_val);
                            break;
                        case type_kw_double:
                            valOne->double_val= valTwo->int_val;
                            LOG_DBG("Moving double literal %d", second->constant.double_val);
                            break;
                        case type_kw_string:
                            valOne->str_val = str_to_str(valTwo->str_val);
                            valOne->isLazy = false;
                            valOne->isStr = true;
                            LOG_DBG("Moving string literal %s", str_to_c_str(second->constant.string_val));
                            break;
                        default:
                            LOG_DBG("ERROR, unknown constant type!", NULL);
                            fail(ERR_INTERNAL, "Unknown constant type", 0, 0);
                    }
                }

                (*curPC)++;

                break;
            case INSTR_FR_CALL:
                LOG_DBG("FR_CALL instruction running", NULL);

                newFrame = vec_frame_ptr_pop(prepFrames);
                newFrame->return_sym = curInstr.first;

                (*curPC)++;

                fr_push(newFrame);

                if(newFrame->function->function.internal)
                {
                    newFrame->function->function.intFun();
                }
                else
                {
                    /* Push the dummy string, to know, where the new frame was created */
                    //sList_push(global_free_later, NULL);
                    curFrame = newFrame;
                    curInstrs = curFrame->function->function.code;
                    curPC = &curFrame->instr_counter;
                    *curPC = 0;
                    curStack = curFrame->local_stack;
                }

                break;
            case INSTR_PREP_VAR:
                LOG_DBG("PREP_VAR instruction running", NULL);

                first = curInstr.first;

                curStack[first->variable.mem_offset].defined = false;

                (*curPC)++;

                break;
            case INSTR_JMP:
                LOG_DBG("JMP instruction running", NULL);

                *curPC += curInstr.offset1 + 1;

                break;
            case INSTR_JMPC:
                LOG_DBG("JMPC instruction running", NULL);

                first = curInstr.first;

                if(first->type == sym_const)
                {
                    switch(first->constant.type)
                    {
                        case type_kw_int:
                            flag = first->constant.int_val != 0;
                            break;
                        case type_kw_double:
                            flag = first->constant.double_val != 0;
                            break;
                        case type_kw_string:
                            flag = strlen(str_to_c_str(first->constant.string_val)) != 0;
                            break;
                        default:
                            LOG_DBG("ERROR, unknown const type");
                            break;
                    }
                }
                else
                {
                    if(curStack[first->variable.mem_offset].defined == false)
                        fail(ERR_UNINITALIZED_VAR, "Cannot jump using undefined variable", 0, 0);
                    switch(first->variable.type)
                    {
                        case type_kw_int:
                            LOG_DBG("Jumping using %d val", curStack[first->variable.mem_offset].int_val);
                            flag = curStack[first->variable.mem_offset].int_val != 0;
                            break;
                        case type_kw_double:
                            flag = curStack[first->variable.mem_offset].double_val != 0;
                            break;
                        case type_kw_string:
                            flag = strlen(str_to_c_str(curStack[first->variable.mem_offset].str_val)) != 0;
                            break;
                        default:
                            LOG_DBG("ERROR, unknown var type");
                            break;
                    }
                }

                if(flag)
                {
                    LOG_DBG("Jumping by %d", curInstr.offset2);
                    *curPC += curInstr.offset2 + 1;
                }
                else
                {
                    LOG_DBG("Jumping by %d", curInstr.offset3);
                    *curPC += curInstr.offset3 + 1;
                }

                break;
            case INSTR_CMP_EQ:
            case INSTR_CMP_NEQ:
            case INSTR_CMP_GT:
            case INSTR_CMP_GEQ:
            case INSTR_CMP_LT:
            case INSTR_CMP_LEQ:
            case INSTR_ADD:
            case INSTR_SUB:
            case INSTR_MUL:
            case INSTR_DIV:
                LOG_DBG("ARITH or LOGIC operation instruction running", NULL);

                first = curInstr.first;
                second = curInstr.second;
                third = curInstr.third;

                mem_item dumMem1, dumMem2;
                valOne = &(curStack[first->variable.mem_offset]);
                LOG_DBG("res : %p", first);
                if(second->type == sym_var)
                {
                    valTwo = &(curStack[second->variable.mem_offset]);
                    LOG_DBG("first : %p", second);
                }
                else
                {
                    dumMem1.defined = true;
                    dumMem1.double_val = second->constant.double_val;
                    valTwo = &dumMem1;
                }
                if(third->type == sym_var)
                {
                    valThree = &(curStack[third->variable.mem_offset]);
                    LOG_DBG("second : %p", third);
                }
                else
                {
                    dumMem2.defined = true;
                    dumMem2.double_val = second->constant.double_val;
                    valThree = &dumMem2;
                }

                if(valTwo->defined == false || valThree->defined == false)
                    fail(ERR_UNINITALIZED_VAR, "Interpretation failed - uninitialized variable used in operation", 0, 0);

                valOne->defined = true;

                switch(curInstr.type)
                {
                    case INSTR_CMP_EQ:
                        if(second->variable.type == type_kw_string)
                        {
                            valOne->int_val = !str_cmp(valTwo->str_val, valThree->str_val);
                        }
                        else
                            OPERATE(valOne, valTwo, ==, valThree, first->variable.type);
                        break;
                    case INSTR_CMP_NEQ:
                        if(second->variable.type == type_kw_string)
                            valOne->int_val = str_cmp(valTwo->str_val, valThree->str_val);
                        else
                            OPERATE(valOne, valTwo, !=, valThree, first->variable.type);
                        break;
                    case INSTR_CMP_GT:
                        OPERATE(valOne, valTwo, >, valThree, first->variable.type);
                        break;
                    case INSTR_CMP_GEQ:
                        OPERATE(valOne, valTwo, >=, valThree, first->variable.type);
                        break;
                    case INSTR_CMP_LT:
                        OPERATE(valOne, valTwo, <, valThree, first->variable.type);
                        LOG_DBG("Res %d < %d is : %d", valTwo->int_val, valThree->int_val, valOne->int_val);
                        break;
                    case INSTR_CMP_LEQ:
                        OPERATE(valOne, valTwo, <=, valThree, first->variable.type);
                        break;
                    case INSTR_ADD:
#if 0
                        OPERATE(valOne, valTwo, +, valThree, first->variable.type);
#endif
                        if(first->variable.type == type_kw_int)
                        {
                            if((valTwo->int_val < 0 && valThree->int_val < INT_MIN - valTwo->int_val) ||
                               (valTwo->int_val > 0 && valThree->int_val > INT_MAX - valTwo->int_val))
                                fail(ERR_RUNTIME_OTHER, "Error, integer overflow/underflow.", 0, 0);

                            valOne->int_val = valTwo->int_val + valThree->int_val;
                        }
                        else
                        {
                            if((valTwo->double_val < 0 && valThree->double_val < DBL_MIN - valTwo->double_val) ||
                               (valTwo->double_val > 0 && valThree->double_val > DBL_MAX - valTwo->double_val))
                                fail(ERR_RUNTIME_OTHER, "Error, double overflow/underflow.", 0, 0);

                            valOne->double_val = valTwo->double_val + valThree->double_val;
                        }
                        break;
                    case INSTR_SUB:
#if 0
                        OPERATE(valOne, valTwo, -, valThree, first->variable.type);
#endif
                        if(first->variable.type == type_kw_int)
                        {
                            if((valTwo->int_val < 0 && valThree->int_val > -INT_MIN + valTwo->int_val) ||
                               (valTwo->int_val > 0 && valThree->int_val < -INT_MAX + valTwo->int_val))
                                fail(ERR_RUNTIME_OTHER, "Error, integer overflow/underflow.", 0, 0);

                            valOne->int_val = valTwo->int_val - valThree->int_val;
                        }
                        else
                        {
                            if((valTwo->double_val < 0 && valThree->double_val > -DBL_MIN + valTwo->double_val) ||
                               (valTwo->double_val > 0 && valThree->double_val < -DBL_MAX + valTwo->double_val))
                                fail(ERR_RUNTIME_OTHER, "Error, integer overflow/underflow.", 0, 0);

                            valOne->double_val = valTwo->double_val - valThree->double_val;
                        }
                        break;
                    case INSTR_MUL:
#if 0
                        OPERATE(valOne, valTwo, *, valThree, first->variable.type);
#endif
                        if(first->variable.type == type_kw_int)
                        {
                            if(valThree->int_val != 0 &&
                               (INT_MAX / valThree->int_val < valTwo->int_val))
                                fail(ERR_RUNTIME_OTHER, "Error, integer overflow/underflow.", 0, 0);

                            LOG_DBG("int %d * %d\n", valTwo->int_val, valThree->int_val);
                            valOne->int_val = valTwo->int_val * valThree->int_val;
                            LOG_DBG("res int : %g\n", valOne->double_val);
                        }
                        else
                        {
                            if(valThree->double_val != 0.0 &&
                               (DBL_MAX / valThree->double_val < valTwo->double_val))
                                fail(ERR_RUNTIME_OTHER, "Error, double overflow/underflow.", 0, 0);

                            LOG_DBG("double %g * %g\n", valTwo->double_val, valThree->double_val);
                            valOne->double_val = valTwo->double_val * valThree->double_val;
                            LOG_DBG("res dbl : %g\n", valOne->double_val);
                        }
                        break;
                    case INSTR_DIV:
                        if(first->variable.type == type_kw_int)
                        {
                            if(valThree->int_val == 0)
                                fail(ERR_DIV_ZERO, "Dividing by zero in expression, or using negative integer!", 0, 0);
                            valOne->int_val = valTwo->int_val / valThree->int_val;
                        }
                        else
                        {
                            if(valThree->double_val == 0.0)
                                fail(ERR_DIV_ZERO, "Dividing by zero in expression, or using negative double!", 0, 0);
                            valOne->double_val = valTwo->double_val / valThree->double_val;
                        }
                        break;
                    default:
                        LOG_DBG("ERROR, unknown instruction found on instruction tape!", NULL);
                }

                (*curPC)++;

                break;
            case INSTR_DBG:
                (*curPC)++;

                break;
            case INSTR_FUN_END:
                LOG_DBG("Function end instruction running", NULL);

                fail(ERR_UNINITALIZED_VAR, "End of function reached, no return found", 0, 0);

                break;
            case INSTR_AND:
            case INSTR_OR:
            case INSTR_XOR:
            case INSTR_NOT:
            default:
                LOG_DBG("ERROR, unknown instruction found on instruction tape!", NULL);
                (*curPC)++;
        }

        if(running)
            curInstr = inter_get_instr(curInstrs, *curPC);
        //fprintf(stderr, "Press enter, to continue!\n");
        //fprintf(stderr, "Instr %d\n", *curPC);
        //getchar();
    }

   // printf("==========================END OF INTERPRET=============================\n");

    vec_frame_ptr_free(prepFrames);
}

void interpret_free()
{

}
