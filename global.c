#include "global.h"
#include "main.h"

sym_tbl* global_sym_table;
vec_symbol_ptr* global_constants;
vec_frame_ptr* global_frames;
//sList* global_free_later;

/*
 * Initialize global variables:
 *   global_sym_table
 *   global_constants
 *   global_frames
 */
int global_init()
{
    global_sym_table = NULL;
    global_constants = NULL;
    global_frames = NULL;

    global_sym_table = st_init();
    if(global_sym_table == NULL)
        fail(ERR_INTERNAL, "Unable to initialize global variables", 0, 0);

    global_constants = vec_symbol_ptr_init();
    if(global_constants == NULL)
        fail(ERR_INTERNAL, "Unable to initialize global variables", 0, 0);

    global_frames = vec_frame_ptr_init();
    if(global_frames == NULL)
        fail(ERR_INTERNAL, "Unable to initialize global variables", 0, 0);

    /*
    global_free_later = sList_init();
    if(global_free_later == NULL)
        fail(ERR_INTERNAL, "Unable to initialize global variables", 0, 0);
        */

    return 1;
}

/*
 * Free global variables:
 *   global_sym_table
 *   global_constants
 *   global_frames
 */
void global_free()
{
    st_free(&global_sym_table);

    //vec_symbol_ptr_foreach(global_constants, sym_debug);
    vec_symbol_ptr_foreach(global_constants, sym_free);
    vec_symbol_ptr_free(global_constants);
    global_constants = NULL;

    vec_frame_ptr_free(global_frames);
    global_frames = NULL;

    /*
    LOG_DBG("Found %ld unfreed \"free_later\" strings!\n", sList_size(global_free_later));
    string s = NULL;
    while(!sList_empty(global_free_later))
    {
        s = sList_pop(global_free_later);
        if(s)
        {
            LOG_DBG("%s\n", str_to_c_str(s));
            tstr_free(s);
        }
        else
            LOG_DBG("NULL\n");
    }
    sList_free(global_free_later);
    global_free_later = NULL;
     */
}

/*
 * Print debug information about runtime frame stack.
 */
void fr_global_debug();

/*
 * Returns a pointer to a new constant symbol.
 *
 * @param Returns a pointer to a new constant symbol
 */
symbol_ptr sym_gen_const()
{
    symbol_ptr s = malloc(sizeof(symbol));

    CHK_ALLOC(s);

    s->type = sym_const;
    vec_symbol_ptr_push(global_constants, s);

    return vec_symbol_ptr_peek(global_constants);
}

/*
 * Returns a pointer to a new temporary symbol.
 *
 * @param Returns a pointer to a new temporary symbol
 */
symbol_ptr sym_gen_temp()
{
    symbol_ptr s = malloc(sizeof(symbol));

    CHK_ALLOC(s);

    s->type = sym_var;
    vec_symbol_ptr_push(global_constants, s);

    return vec_symbol_ptr_peek(global_constants);
}

/*
 * Generate symbol for internal function.
 *
 * @param retType Return type of internal function
 * @param paramNum Number of parameters.
 * @param fun Function to call.
 * @return Returns the required symbol.
 */
symbol_ptr sym_gen_ifunc(token_type retType, int paramNum, void(*fun)(void))
{
    symbol_ptr s = malloc(sizeof(symbol));

    CHK_ALLOC(s);

    s->type = sym_function;
    s->function.internal = true;
    s->function.defined = true;
    s->function.intFun = fun;
    s->function.name = NULL;
    s->function.params = vec_symbol_ptr_init();
    s->function.ret_type = retType;
    s->function.stack_size = paramNum;

    return s;
}

/*
 * Returns a pointer to a new function symbol.
 * Needs to be explicitly added to the global table.
 *
 * @return Returns a pointer to a new function symbol
 */
symbol_ptr sym_gen_func()
{
    symbol_ptr s = malloc(sizeof(symbol));

    CHK_ALLOC(s);

    s->type = sym_function;
    s->function.code = vec_instr_init();
    s->function.name = NULL;
    s->function.params = vec_symbol_ptr_init();
    s->function.ret_type = -1;
    s->function.stack_size = 1;
    s->function.defined = false;
    s->function.internal = false;

    return s;
}

/*
 * Frees the structure of the given function symbol, including its internal structures.
 *
 * @param sym Function symbol to free.
 */
void sym_free_func(symbol_ptr sym)
{
    if(sym->function.params)
        vec_symbol_ptr_free(sym->function.params);
    if(sym->function.code)
        vec_instr_free(sym->function.code);
    free(sym);
}

/*
 * Returns a pointer to a new variable symbol.
 * Needs to be explicitly added to the global table.
 *
 * @return Returns a pointer to a new variable symbol
 */
symbol_ptr sym_gen_var()
{
    symbol_ptr s = malloc(sizeof(symbol));

    CHK_ALLOC(s);

    s->type = sym_var;

    return s;
}

/*
 * Search for given function in the global symbol table.
 *
 * @param name Name of the function.
 * @return Returns a pointer to the function symbol, or NULL, if the variable was not found.
 */
symbol_ptr lookup_fun(string name)
{
    return  st_lookup(global_sym_table, name);
}

/*
 * Search for given variable, only in the given scope. Searches in the global symbol table.
 *
 * @param name Name of the searched variable.
 * @param scope Scope of the searched variable.
 * @return Returns Symbol of the searched variable or NULL, if the variable was not found.
 */
symbol_ptr lookup_var_local(string name, string scope)
{
    string fullName = str_concat(name, scope);

    symbol_ptr found = st_lookup(global_sym_table, fullName);

    tstr_free(fullName);
    
    return found;
}

/* Will look into global table and search for variable in local scope, and all encapsulating scopes*/
/*
 * Search for given variable. The search starts at given scope and ends at the global scope.
 *
 * @param name Name of the searched variable.
 * @param scope Starting scope of the searched variable.
 * @return Returns Symbol of the searched variable or NULL, if the variable was not found.
 */
symbol_ptr lookup_var_global(string name,string scope)
{
    LOG_DBG("name : %p | scope : %p\n", name, scope);
    LOG_DBG("name->data : %p | scope->data : %p\n", name->data, scope->data);
    string fullName = str_concat(name, scope);
    symbol_ptr found = st_lookup(global_sym_table, fullName);

    //while(found == NULL && str_replace_last(fullName, '.', '\0'))
    while(found == NULL && str_delete_downto(fullName, '.'))
        found = st_lookup(global_sym_table, fullName);

    tstr_free(fullName);
    return found;
}


bool function_signatures_equal(symbol_ptr a, symbol_ptr b)
{
    if(a && b && a->type == sym_function && b->type == sym_function
        && a->function.ret_type  == b->function.ret_type
        && a->function.params && b->function.params
        && vec_symbol_ptr_size(a->function.params) ==  vec_symbol_ptr_size(b->function.params) )
    {
        bool ret = true;
        for (int i = 0; i < vec_symbol_ptr_size(a->function.params) && ret; ++i)
        { 
            symbol_ptr param_a = a->function.params->data[i];
            symbol_ptr param_b = b->function.params->data[i];

            if(param_a->type != sym_var || param_b->type != sym_var ||
                param_a->variable.type != param_b->variable.type)
                ret =false;

            if(str_cmp(param_a->variable.name, param_b->variable.name) != 0)
                ret = false;

        }
        return ret;

    }

    return false;
}
