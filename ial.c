#include "ial.h"
#include "main.h"

/* These function are only used for table managment, no point in having them in symbol.h*/

symbol_el_ptr s_init(const char* key, symbol_ptr data);
symbol_el_ptr s_init_str(const string key, symbol_ptr data);
symbol_el_ptr _s_init_str(string key, symbol_ptr data);

void s_free(symbol_el_ptr symbol);

/*
 * Initializes given symbol element, using c-style string.
 *
 * @param key String, symbolizing name of the symbol.
 * @return Returns the symbol item pointer, or NULL, in case of error.
 */
symbol_el_ptr s_init(const char* key, symbol_ptr data)
{
    string newKey = c_str_to_str(key);

    return _s_init_str(newKey, data);
}

/*
 * Initialize given symbol element, using a string.
 *
 * @param key String, symbolizing name of the symbol.
 * @return Returns the symbol item pointer, or NULL, in case of error.
 */
symbol_el_ptr s_init_str(const string key, symbol_ptr data)
{
    string newKey = str_to_str(key);

    return _s_init_str(newKey, data);
}


/*
 * Inner function, uses the key, without allocating new string.
 *
 * @param key String key.
 * @param data Data of the element.
 * @return Returns symbol element, with given key and data. Must be freed.
 */
symbol_el_ptr _s_init_str(string key, symbol_ptr data)
{
    symbol_el_ptr newEl = malloc(sizeof(symbol_el));
    CHK_ALLOC(newEl);
    newEl->key = key;
    newEl->data = data;
    newEl->next = NULL;

    return newEl;
}

/*
 * Frees the inside of given symbol item. !! Does not free the symbol item struct itself !!
 *
 * @param symbol Symbol to free.
 */
void s_free(symbol_el_ptr symbol)
{
    tstr_free(symbol->key);
    sym_free(symbol->data);
    free(symbol);
}


/* Knuth-Morris-Pratt substring search algorithm */

/*
 * Generate prefix table required for KMP algorithm.
 *
 * @param pattern Pattern used for generating the prefix table.
 * @param length Length of the matching part of the patter. ('\0' excluded)
 * @param pTable Allocated structure, with <length> size. Will contain the output.
 */
void kmp_gen_prefix(const char* pattern, int64_t length, int* pTable)
{
    pTable[0] = -1;

    for(int k = 1, r = -1; k < length; ++k)
    {
        while (r > -1 && pattern[r + 1] != pattern[k])
            r = pTable[r];
        if (pattern[k] == pattern[r + 1])
            r++;
        pTable[k] = r;
    }
}

/*
 * Uses the Knuth-Morris-Pratt algorithm to search for the pattern in the sentence.
 *
 * WARNING: Takes the whole vector <0 , size - 1>. Including '\0'.
 *
 * @param heap The sentence being searched for the pattern.
 * @param patt The pattern to be found in the sentence.
 * @return Returns the index of first found occurrence of pattern in the sentence (starting at 0!).
 *           If no matches were found, returns -1 instead.
 */
int64_t kmp_search(const string heap, const string patt)
{
    int64_t sLen = tstr_size(heap);
    const char* sentence = heap->data;
    int64_t pLen = tstr_size(patt);
    const char* pattern = patt->data;

    int* pTable = malloc(sizeof(int) * pLen);
    CHK_ALLOC(pTable);

    kmp_gen_prefix(pattern, pLen, pTable);

    int sInd = 0;
    int pInd = 0;

    while((sInd < sLen) && (pInd < pLen))
    {
        // Found a matching character
        if((pInd == -1) || (sentence[sInd] == pattern[pInd]))
        {
            sInd++;
            pInd++;
        }
        else /* Didn't match whole pattern */
            pInd = pTable[pInd + 1];
    }

    free(pTable);

    if(pInd >= pLen)
        return sInd - pLen;
    else
        return -1;
}

/* Shell sort */

/*
 * Shell sort implementation, used for sorting character arrays.
 *
 * @param array Pointer to the character array.
 * @param size Size of given array.
 */
void shell_sort(char* array, int64_t size)
{
    for(int64_t step = size / 2;
        step > 0;
        step /= 2)
    {
        for(int64_t ind1 = step;
            ind1 < size;
            ++ind1)
        {
            for(int64_t ind2 = ind1 - step;
                ind2 >= 0 && array[ind2] > array[ind2 + step];
                ind2 -= step)
            {
                char temp = array[ind2];
                array[ind2] = array[ind2 + step];
                array[ind2 + step] = temp;
            }
        }
    }
}

/* Hash table */

const uint32_t ST_START_SIZE = 193;

/*
 * Initialize symbol table. Starting size is ST_START_SIZE.
 *
 * @return Returns initialized symbol table, or NULL, if initialization failed.
 */
sym_tbl* st_init()
{
    sym_tbl * newTable = malloc(sizeof(sym_tbl));
    CHK_ALLOC(newTable);
    newTable->capacity = ST_START_SIZE;
    newTable->filled = 0;
    newTable->table = malloc(sizeof(symbol_el_ptr) * ST_START_SIZE);
    CHK_ALLOC(newTable->table);
    memset(newTable->table, 0, sizeof(symbol_el_ptr) * ST_START_SIZE);

    return newTable;
}

/*
 * Prints debug information about the symbol table.
 *
 * @param st Input symbol table
 */
void st_info(sym_tbl* st)
{
#ifdef DEBUG
    int emptyStart = -1;

    for(uint32_t iii = 0; iii < st->capacity; ++iii)
    {
        if(!st->table[iii])
        {
            if(emptyStart == -1)
                emptyStart = iii;
        }
        else
        {
            if(emptyStart != -1)
            {
                printf("<%d -> %d> : EMPTY\n", emptyStart, iii - 1);
                emptyStart = -1;
            }

            printf("<%d> : FILLED\n", iii);

            symbol_el_ptr first = st->table[iii];
            while(first)
            {
                puts("          /\\           ");
                puts("/--------------------->");
                printf("| Name : \t< %s >\n", str_to_c_str(first->key));
                printf("| SymbolType : \t< %d >\n", first->data->type);
                printf("| Next : \t< %p >\n", (void*)first->next);
                puts("\\--------------------->");
                first = first->next;
            }

        }
    }
    if(emptyStart != -1)
        printf("<%d -> %d> : EMPTY\n", emptyStart, st->capacity - 1);
#endif
}

/*
 * Destroys the given symbol table. Changes the given pointer to NULL.
 *
 * @param st Symbol table to be destroyed.
 */
void st_free(sym_tbl** st)
{
    sym_tbl* stPtr = *st;
    for(uint64_t iii = 0; iii < stPtr->capacity; ++iii)
    {
        symbol_el_ptr first = stPtr->table[iii];
        symbol_el_ptr next = NULL;

        while (first)
        {
            next = first->next;
            s_free(first);
            first = next;
        }

        stPtr->table[iii] = NULL;
    }

    free(stPtr->table);
    free(stPtr);
    *st = NULL;
}

/*
 * Run given function for each symbol in given symbol table.
 *
 * @param st Symbol table to operate on.
 * @param fun Function to execute.
 */
void st_foreach(sym_tbl* st, void(*fun)(symbol_ptr))
{
    for(uint64_t iii = 0; iii < st->capacity; ++iii)
    {
        symbol_el_ptr first = st->table[iii];
        symbol_el_ptr next = NULL;

        while (first)
        {
            next = first->next;
            fun(first->data);
            first = next;
        }
    }
}

/*
 * Get index in hash table, using hash function.
 *
 * @param st Initialized symbol table.
 * @param symbol Indexed symbol.
 * @return Returns index in the given symbol table.
 */
int64_t st_get_index(sym_tbl* st, string key)
{
#ifdef USE_DJB2
    return _djb2((const unsigned char*)str_to_c_str(key)) % st->capacity;
#else
    return _sdbm((const unsigned char*)str_to_c_str(key)) % st->capacity;
#endif
}

/*
 * Look for the symbol with given key.
 *
 * @param st Initialized symbol table.
 * @param key Identification of searched object.
 * @return Returns first found item, with the same key. Returns NULL, if no item was found.
 */
symbol_ptr st_lookup(sym_tbl* st, string key)
{
    symbol_el_ptr first = st->table[st_get_index(st, key)];

    while(first)
    {
        // If the keys match
        if(str_cmp(first->key, key) == 0)
            return first->data;
        first = first->next;
    }

    return NULL;
}

/*
 * Inserts given symbol into the symbol table.
 *
 * Also performs a check, if there already is a symbol with the same key. Returns NULL, if the symbol is found.
 *
 * @param st Initialized symbol table.
 * @param symbol Symbol being inserted.
 * @return Returns pointer to the newly inserted element, if successful. Else returns NULL.
 */
symbol_el_ptr st_insert(sym_tbl* st, const symbol_ptr data, const string key)
{
    symbol_el_ptr* line = &(st->table[st_get_index(st, key)]);
    symbol_el_ptr first = *line;

    while (first)
    {
        // If the keys match
        if (str_cmp(first->key, key) == 0)
            return NULL;
        if (!first->next)
            break;
        first = first->next;
    }

    // Insert the new symbol element

    symbol_el_ptr newElem = s_init_str(key, data);

    // Add the new element as the first one
    newElem->next = *line;
    *line = newElem;

    /*
    if(*line)
        first->next = newElem;
    else
        *line = newElem;
    */

    return newElem;
}
