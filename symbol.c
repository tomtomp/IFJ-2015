
#include "symbol.h"

#define VEC_DATA_TYPE symbol_ptr
#define VEC_DEFAULT_VAL 0
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

#define VEC_DATA_TYPE symbol
#define VEC_DEFAULT_VAL {0}
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

/*
 * Free given symbol.
 *
 * @param sym Symbol to free
 */
void sym_free(symbol_ptr sym)
{
    switch(sym->type)
    {
        case sym_const:
            if(sym->constant.type == type_kw_string)
                tstr_free(sym->constant.string_val);

            break;
        case sym_function:
            if(sym->function.params)
            {
                if(sym->function.internal)
                    vec_symbol_ptr_foreach(sym->function.params, sym_free);
                vec_symbol_ptr_free(sym->function.params);
            }
            if(sym->function.code && !sym->function.internal)
                vec_instr_free(sym->function.code);
            if(sym->function.name)
                tstr_free(sym->function.name);
            break;
        case sym_var:
            if(sym->variable.name)
            {
                tstr_free(sym->variable.name);
            }
            break;
    }

    free(sym);
}

/*
 * Print debug info about given symbol.
 *
 * @param sym Symbol to print debug info about
 */
void sym_debug(symbol_ptr sym)
{
#ifdef DEBUG
    printf("===================SYMBOL===================\n");
    if(sym == NULL)
    {
        printf("Given symbol is NULL!\n");
        return;
    }

    switch(sym->type)
    {
        case sym_var:
            printf("Symbol type: variable\n");
            switch(sym->variable.type)
            {
                case type_kw_int:
                    printf("Type : integer\n");
                    break;
                case type_kw_double:
                    printf("Type : double\n");
                    break;
                case type_kw_string:
                    printf("Type : string\n");
                    break;
                default:
                    printf("Unknown variable type!\n");
                    break;
            }

            printf("Symbol offset in runtime frame: %d\n", sym->variable.mem_offset);
            break;
        case sym_const:
            printf("Symbol type: const\n");
            switch(sym->constant.type)
            {
                case type_kw_int:
                    printf("Type : integer\n");
                    printf("  Value : %d\n", sym->constant.int_val);
                    break;
                case type_kw_double:
                    printf("Type : double\n");
                    printf("  Value : %e\n", sym->constant.double_val);
                    break;
                case type_kw_string:
                    printf("Type : string\n");
                    if(sym->constant.string_val)
                        printf("  Value : %s\n", str_to_c_str(sym->constant.string_val));
                    else
                        printf("  Error, pointer is NULL!\n");
                    break;
                default:
                    printf("Unknown constant type!\n");
                    break;
            }
            break;
        case sym_function:
            printf("Symbol type: function\n");

            if(sym->function.code && !sym->function.internal)
            {
                printf("Code size: %ld\n", vec_instr_size(sym->function.code));
                printf("Code dump: \n");
                vec_instr_foreach_ptr(sym->function.code, instr_debug);
                printf("Code dump done!\n");
            }
            else
                printf("No code/internal function!\n");

            if(sym->function.params)
            {
                printf("Num of parameters: %ld\n", vec_symbol_ptr_size(sym->function.params));
                printf("Parameter dump: \n");
                vec_symbol_ptr_foreach(sym->function.params, sym_debug);
                printf("Parameter dump done!\n");
            }
            else
                printf("No params!\n");

            switch(sym->function.ret_type)
            {
                case type_kw_int:
                    printf("Type : integer\n");
                    break;
                case type_kw_double:
                    printf("Type : double\n");
                    break;
                default:
                    printf("Unknown function return type!\n");
                    break;
            }

            printf("Stack size: %d\n", sym->function.stack_size);
            break;
        default:
            printf("Unknown symbol type!\n");
            break;
    }
    printf("===================END===================\n");
#endif
}
