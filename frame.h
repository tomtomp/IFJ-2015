#ifndef FRAME_H
#define FRAME_H
#include "symbol.h"


typedef struct TStackItem
{
    union
    {
        int int_val;
        double double_val;
        string str_val;
    };

    // Set to true, if the memory cell holds a string
    bool isStr;
    // Set to true, if the memory cell holds lazily initialized string - only pointer, no copy
    bool isLazy;

    bool defined;
} mem_item;

typedef struct TStackFrame
{
    /* Return symbol, on previous function's stack, where return value should be moved */
    symbol_ptr return_sym;
    /* Pointer to currently executed function */
    symbol_ptr function;
    /* Counter of instructions, we need to store this in frame for recursion */
    int instr_counter;
    /*  Actual data stack for storing variable values, needs to be at least 
        function.stack_size elements big 
    */
    mem_item* local_stack;
} frame;

typedef frame* frame_ptr;

#define VEC_DATA_TYPE frame_ptr
#define VEC_DEFAULT_VAL {0}
#include "vec_base.ht"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE

/*
 * Allocates and initializes a new frame. Local stack will be created according to the size given in function symbol.
 *
 * @param function Function symbol pointer, used for getting information about the frame.
 * @param ret_sym Return symbol, used for a pointer to preceding frame, can be NULL and set later.
 * @return Returns malloced frame, or NULL, if the function failed.
 */
frame_ptr fr_new(symbol_ptr function, symbol_ptr ret_sym);

/*
 * Frees given frame structure. Includes freeing the local stack.
 *
 * @param fr Frame to be freed
 */
static inline void fr_free(frame_ptr fr)
{
    mem_item* del = NULL;
    int size = fr->function->function.stack_size;
    for(int iii = 0; iii < size; ++iii)
    {
        del = &fr->local_stack[iii];
        if(del->isStr && del->defined && !del->isLazy)
            tstr_free(fr->local_stack[iii].str_val);
    }
    free(fr->local_stack);
    free(fr);
}

/*
 * Print debug information about given frame.
 *
 * @param fr Frame to process
 */
void fr_debug(frame_ptr fr);

#endif