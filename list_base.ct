
#include <stdint.h>
#include <string.h>

/*
 * Create new 2-way list.
 *
 * @return Returns ready to use list.
 */
_LIST_TYPE * LIST_INIT_FUN(LIST_DATA_TYPE)()
{
    _LIST_TYPE * newList = malloc(sizeof(_LIST_TYPE));

    CHK_ALLOC(newList);

    newList->first = NULL;
    newList->last = NULL;
    newList->active = NULL;
    newList->size = 0;

    return newList;
}

/*
 * Free the given list and all of its elements.
 *
 * @param list List to free.
 */
void LIST_FREE_FUN(LIST_DATA_TYPE)(_LIST_TYPE * list)
{
    _LIST_ELEM_TYPE *actElem = list->first;
    _LIST_ELEM_TYPE *temp = NULL;

    while(actElem)
    {
        temp = actElem;
        actElem = actElem->rPtr;
        free(temp);
    }

    free(list);
}

/*
 * Insert new data on the front of the list.
 *
 * @param list List to operate on.
 * @param data Data to insert.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_INSERT_FIRST_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, LIST_DATA_TYPE data)
{
    _LIST_ELEM_TYPE * newElem = LIST_GEN_ELEM_FUN(LIST_DATA_TYPE)();

    newElem->lPtr = NULL;
    newElem->rPtr = list->first;
    newElem->data = data;

    if(list->last == NULL)
        list->last = newElem;
    else
        list->first->lPtr = newElem;

    list->first = newElem;

    list->size++;

    return list;
}

/*
 * Insert new data on the end of the list.
 *
 * @param list List to operate on.
 * @param data Data to insert.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_INSERT_LAST_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, LIST_DATA_TYPE data)
{
    _LIST_ELEM_TYPE * newElem = LIST_GEN_ELEM_FUN(LIST_DATA_TYPE)();

    newElem->lPtr = list->last;
    newElem->rPtr = NULL;
    newElem->data = data;

    if(list->first == NULL)
        list->first = newElem;

    list->last = newElem;

    list->size++;

    return list;
}

/*
 * Insert new data before the active element.
 *
 * @param list List to operate on.
 * @param data Data to insert.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_PRE_INSERT_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, LIST_DATA_TYPE data)
{
    if(list->active)
    {
        _LIST_ELEM_TYPE * newElem = LIST_GEN_ELEM_FUN(LIST_DATA_TYPE)();

        newElem->lPtr = list->active->lPtr;
        newElem->rPtr = list->active;
        newElem->data = data;

        if(list->first == list->active)
            list->first = newElem;
        else
            list->active->lPtr->rPtr = newElem;

        list->active->lPtr = newElem;

        list->size++;
    }

    return list;
}

/*
 * Insert new data after the active element.
 *
 * @param list List to operate on.
 * @param data Data to insert.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_POST_INSERT_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, LIST_DATA_TYPE data)
{
    if(list->active)
    {
        _LIST_ELEM_TYPE * newElem = LIST_GEN_ELEM_FUN(LIST_DATA_TYPE)();

        newElem->lPtr = list->active;
        newElem->rPtr = list->active->rPtr;
        newElem->data = data;

        if(list->last == list->active)
            list->last = newElem;
        else
            list->active->rPtr->lPtr = newElem;

        list->active->rPtr = newElem;

        list->size++;
    }

    return list;
}

/*
 * Pops the first element off the list. If the list is empty, nothing happens.
 *
 * @param list List to operate on.
 * @return Returns the required data, or default value, if the list is empty.
 */
LIST_DATA_TYPE LIST_POP_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list)
{
    if(list->first)
    {
        _LIST_ELEM_TYPE *delElem = list->first;
        LIST_DATA_TYPE data = delElem->data;
        if(list->first == list->last)
            list->last = NULL;
        else
            list->first->rPtr->lPtr = NULL;
        list->first = list->first->rPtr;

        free(delElem);

        list->size--;

        return data;
    }
    else
    {
        LIST_DATA_TYPE data = LIST_DEFAULT_VAL;
        return data;
    }
}

/*
 * Gets the data from the first element in the list.
 *
 * @param list List to operate on.
 * @return Returns required data, or default value, if the list is empty.
 */
LIST_DATA_TYPE LIST_TOP_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list)
{
    if(list->first)
        return list->first->data;
    else
    {
        LIST_DATA_TYPE data = LIST_DEFAULT_VAL;
        return data;
    }
}

/*
 * Run the given function for each data in the list.
 *
 * @param list List to operate on.
 * @param fun Function to run.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_FOREACH_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, void(*fun)(LIST_DATA_TYPE))
{
    _LIST_ELEM_TYPE *iter = list->first;

    while(iter)
    {
        fun(iter->data);
        iter = iter->rPtr;
    }

    return list;
}

/*
 * Run the given function for each data (as pointer) in the list.
 *
 * @param list List to operate on.
 * @param fun Function to run.
 * @return Returns the input list, or NULL, if error occurred.
 */
_LIST_TYPE * LIST_FOREACH_PTR_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list, void(*fun)(LIST_DATA_TYPE*))
{
    _LIST_ELEM_TYPE *iter = list->first;

    while(iter)
    {
        fun(&iter->data);
        iter = iter->rPtr;
    }

    return list;
}

/*
 * Gets the data from the active element. If no elements are active, gets default value instead.
 *
 * @param list List to operate on.
 * @return Returns required data, or default value, if the list is empty.
 */
LIST_DATA_TYPE LIST_GET_FUN(LIST_DATA_TYPE)(_LIST_TYPE *list)
{
    if(list->active)
        return list->active->data;
    else
    {
        LIST_DATA_TYPE data = LIST_DEFAULT_VAL;
        return data;
    }
}
