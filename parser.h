#ifndef PARSER_H
#define PARSER_H

#include "main.h"
#include "scanner.h"
#include "symbol.h"

#define LIST_CUSTOM_NAME oStack
#define LIST_DATA_TYPE int
#define LIST_DEFAULT_VAL 0
#include "list_base.ht"
#undef LIST_DEFAULT_VAL
#undef LIST_DATA_TYPE
#undef LIST_CUSTOM_NAME

#define is_token_terminal(t) (t.type <= type_eof)
#define is_token_nonterminal(t) (t.type > type_eof)
#define is_token_kw(t) ((t.type >= type_kw_auto) && (t.type <= type_kw_string))
#define is_token_type(t) (t.type >= type_kw_int && t.type <= type_kw_double)
#define next_token_expect(t,typ) ((t=next()).type == typ)
#define this_token_expect(t,typ) ((t=token).type == typ)
#define expr_type_is_op(type) ((type) >= expr_op_3 && (type) <= expr_op_9)

typedef enum
{
    expr_mark = -3, // Used for marking, when shifting
    expr_non_term_var = -2,
    expr_non_term = -1,
    expr_var = 0, //encapsulates variable identifier and const, resolving will be done during code gen
    expr_fun,
    expr_bracket_open,
    expr_bracket_close,
    expr_op_3,
    expr_op_6,
    expr_op_9,
    expr_comma,
    expr_end,
    expr_size
} expr_type ;

typedef struct TExprSym
{
    expr_type type;
    token_type tokType;
    symbol_ptr sym;
} expr_sym;

#define LIST_CUSTOM_NAME eStack
#define LIST_DATA_TYPE expr_sym
#define LIST_DEFAULT_VAL {expr_size, type_eof, NULL}
#include "list_base.ht"
#undef LIST_DEFAULT_VAL
#undef LIST_DATA_TYPE
#undef LIST_CUSTOM_NAME

#define SHIFT 1
#define REDUC 2
#define MATCH 3
#define PTERR 4
#define PTDON 5
#define PTFUN 6
#define DEFUN 7
#define THROW 8

/* Only for testing, definition should be private to this file */
symbol_ptr parse_expr(int end1, int end2);
/* end */

bool parser_init();
void parser_free();
void parse();

void program();
void fun_dd();
void param();
void param_l();

void f_spec();
void cmp_st();
void st_list();
void st();
void st_assign(token_type);
void var_dd();
void st_in();
void st_in_l();

void st_out();
void st_out_l();

void st_if();
void st_for();
void st_while();
void st_dowhile();





void cg_init();
//Called when encountering new function signature 
void cg_new_fun(token_type type, string name);
//Called when encountering new function param
void cg_fun_param(token_type type, string name);
/*  Called when function is defined, function should check for previous declarations,
    compare signatures , and all further code generation functions should modify this
    function's data (generation of temp variables, appen ding instructions )*/
void cg_fun_defined();
/*  Called when function is declared, 
    this function should check for previous declarations of function with same names */
void cg_fun_declared();
/*  Called when function definition/declaration has ended, function should save all
    necessary info and clean temp variables*/
void cg_fun_ended();

/*  Called when entering new scope, function should create scope name and append it 
    to full scope name */
void cg_scope_enter();
/*  Called when exiting scope, function should remove last part from full scope name */
void cg_scope_exit();
/* etc ....*/


void cg_assign(string name,symbol_ptr);
void cg_var_define(string name,token_type,symbol_ptr);
void cg_var_declare(string name,token_type);
void cg_output(symbol_ptr);
void cg_input(string name);

void cg_for_def();//called after 1st part of for has been parsed
void cg_for_cond(symbol_ptr);
void cg_for_expr(symbol_ptr);
void cg_for_ended();

void cg_return(symbol_ptr);

void cg_if_cond(symbol_ptr);
void cg_if_positive();
void cg_if_negative();
void cg_if_ended();


/*
 * Generate a new temporary variable and return the pointer to it.
 *
 * @return Returns new temporary variable inside current function.
 */
symbol_ptr cg_gen_temp_var();

/*
 * Search for a symbol in current and enveloping scopes.
 *
 * @param identifier Name of the symbol.
 * @return Returns pointer to a symbol in current or enveloping scope, or NULL, if no symbol with given ID exists.
 */
symbol_ptr cg_get_var(string identifier);

/*
 * Generate arithmetic instruction of given type, using the operands supplied.
 *
 * @param res Where to store result.
 * @param op1 First operand.
 * @param op2 Second opearnd.
 * @param op Operation, for example - "type_plus".
 * @param buffer Where to add the instruction.
 * @return Returns the symbol containing result.
 */
symbol_ptr cg_gen_ar_instr(symbol_ptr res, symbol_ptr op1, symbol_ptr op2, token_type op, vec_instr* buffer);

/* 
 * If types are equal, returns src, if types are convertable, appends convert instruction to buffer, else fails 
 */
symbol_ptr type_match(token_type dest_type, symbol_ptr src,vec_instr* buffer);

/*
 * Get the type of result of arithmetic operation with given operands.
 *
 * @param op1 First operand.
 * @param op2 Second operand.
 * @return Returns resulting type. If unable to resolve the type, returns type_eof.
 */
token_type get_res_type(symbol_ptr op1, symbol_ptr op2);

/*
 * Free resources used by code generation
 */
void cg_free();



#endif
