#include "string.h"

#define VEC_CUSTOM_NAME tstr
#define VEC_DATA_TYPE char
#define VEC_DEFAULT_VAL (0)
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE
#undef VEC_CUSTOM_NAME

typedef tstr* string;

#define VEC_CUSTOM_NAME sList
#define VEC_DATA_TYPE string
#define VEC_DEFAULT_VAL (NULL)
#include "vec_base.ct"
#undef VEC_DEFAULT_VAL
#undef VEC_DATA_TYPE
#undef VEC_CUSTOM_NAME

/* Space for definitions of tstr specific functions */

/*
 * Initialize string from C-style string. Includes the terminal '\0'!!
 *
 * @param str C-style, '\0' terminated character vector.
 * @return Initialized string, needs to be freed;
 */
string c_str_to_str(const char* str)
{
    size_t length = strlen(str);

    string vec = malloc(sizeof(tstr));
    CHK_ALLOC(vec);
    vec->data = malloc(length);
    CHK_ALLOC(vec->data);
    vec->size = length;
    vec->capacity = length;

    strncpy(vec->data, str, length);

    return vec;
}

/*
 * Initialize string from given string
 *
 * @param str Input string
 * @return Returns new string, needs to be freed!
 */
string str_to_str(const string str)
{
    string vec = malloc(sizeof(tstr));
    CHK_ALLOC(vec);
    vec->data = malloc(str->size);
    CHK_ALLOC(vec->data);
    vec->size = str->size;
    vec->capacity = str->size;

    //strncpy(vec->data, str->data, str->size);
    memcpy(vec->data, str->data, str->size);

    return vec;
}

/*
 * Take string (character vector) and return c-type string. Must be called each time before the pointer is used!!
 *
 * Does NOT create new space for the char[], returns pointer to given strings data!
 *
 * @param str Input string
 * @return C-type string
 */
const char* str_to_c_str(string str)
{
    //if(tstr_full(str) || str->data[str->size] != '\0')
    {
        // Adds the terminal \0
        tstr_push(str, '\0');
        // Returns the top pointer to the original position
        tstr_pop(str);
    }

    return (str->data);
}

/*
 * Create c style string from char vector, COPY contents and add trailing 0
*/

char* str_copy_c_str(string str)
{
    char* res = malloc(sizeof(char)*(str->size+1));
    CHK_ALLOC(res);
    strncpy(res,str_to_c_str(str),str->size)  ;
    return res;
}
/*
 * Compare two strings (character vectors). Returns zero, if the strings match.
 *
 * Uses strncmp function from the standard library.
 *
 * @param str1 First string.
 * @param str2 Second string.
 * @return Returns zero, if the strings match.
 */
int32_t str_cmp(string str1, string str2)
{
    if(str1->size != str2->size)
        return -1;

    if(str1->size < 0)
        return -1;

    return strncmp(str1->data, str2->data, str1->size);
}

/*
 * Return new string, containing str1, concatenated with str2.
 * Resulting string will ignore the first strings ending '\0' and add one to the end of
 *   second string, if needed.
 *
 * @param str1 First string
 * @param str2 Second string
 * @return Returns new string -> Needs to be tstr_freed!
 */
string str_concat(string str1, string str2) {
    string strConcat = tstr_init();

    tstr_append(strConcat, str1);

    if (tstr_peek(strConcat) == '\0')
        tstr_pop(strConcat);

    tstr_append(strConcat, str2);

    if(tstr_peek(strConcat) != '\0')
    {
        tstr_push(strConcat, '\0');
        tstr_pop(strConcat);
    }

    return strConcat;
}

/*
 * Get substring of given string. Similar function to basic_string::substr in C++11.
 *
 * @param str String to use.
 * @param start Index to start substring at. Starts at 0.
 * @param num Number of characters to copy.
 * @return Returns new string containing requested substring.
 */
string str_substr(string str, int start, int num)
{
    string newStr = tstr_init();

    int64_t stop = start + num;
    int64_t origSize = tstr_size(str);

    if(origSize < stop)
        stop = origSize;

    int64_t newNum = stop - start;

    tstr_make_space(newStr, newNum + 1);
    memcpy(newStr->data, str->data + start, newNum);
    newStr->size = newNum;
    str_to_c_str(newStr);

    return newStr;
}

/*
 * Replaces the last occurrence of the given character by the given character.
 *
 * @param str String to operate on.
 * @param s Char to replace.
 * @param rep What to replace it with.
 * @return Returns a pointer to the replaced character, or a NULL, if unable to replace.
 */
char* str_replace_last(string str, char s, char rep)
{
    if(tstr_full(str) || str->data[str->size] != '\0')
    {
        // Adds the terminal \0
        tstr_push(str, '\0');
        // Returns the top pointer to the original position
        tstr_pop(str);
    }

    char* found = strrchr(str->data, s);

    if(found == NULL)
        return NULL;

    *found = rep;
    return found;
}

/*
 * Deletes from the end of the string down to (including) the given character.
 *
 * @param str String to operate on.
 * @param s Character to delete down to.
 * @return Returns pointer to the character, or NULL, if the character was not found.
 */
char* str_delete_downto(string str, char s)
{
    //if(tstr_full(str) || str->data[str->size] != '\0')
    {
        // Adds the terminal \0
        tstr_push(str, '\0');
        // Returns the top pointer to the original position
        tstr_pop(str);
    }

    char* found = strrchr(str->data, s);

    if(found == NULL)
        return NULL;

    //int64_t length = str->data + str->size - found;
    int64_t length =str->size - (found - str->data);
    str->size -= length;

    return found;
}

/*
 * Print content of string
 *
 * @param str1 String to be printed
 */
void str_print(string str1)
{
    for(int iii = 0; iii < tstr_size(str1); ++iii)
        putc(*tstr_at(str1, iii), stdout);
    putc('\n', stdout);

    for(int iii = 0; iii < tstr_size(str1); ++iii)
        printf("%c(%d)\n", *tstr_at(str1, iii), *tstr_at(str1, iii));
    putc('\n', stdout);
}

